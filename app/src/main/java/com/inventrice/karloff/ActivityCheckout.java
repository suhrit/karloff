package com.inventrice.karloff;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.inventrice.karloff.Util.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by suhrit on 21-10-2017.
 */

public class ActivityCheckout extends AppCompatActivity {

    TextView error,pintxt,mobileno,orderid,netPrice,total,orderTxt,error2;
    public String name = null,email=null,phn=null, pin = null, landmark = null, h_no = null,st_name = null;
    SharedPreferences prefs;
    String orderId,totalVal,pinStat;
    LinearLayout checkout;
    ProgressDialog pd;
    CoordinatorLayout main;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_checkout);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //What to do on back clicked
                finish();
            }
        });

        prefs = getSharedPreferences("login_pref", MODE_PRIVATE);


        name = prefs.getString("name", "NULL");//"No name defined" is the default value.
        email = prefs.getString("email", "NULL"); //0 is the default value.
        phn = prefs.getString("phn", "NULL");
        pin = prefs.getString("pincode", "NULL");
        landmark = prefs.getString("landmark", "NULL");
        h_no = prefs.getString("h_no", "NULL");
        st_name = prefs.getString("st_name", "NULL");

        error = (TextView) findViewById(R.id.error);
        error2 = (TextView) findViewById(R.id.error2);
        pintxt = (TextView) findViewById(R.id.pintxt);
        mobileno = (TextView) findViewById(R.id.mobileno);
        orderid = (TextView) findViewById(R.id.orderid);
        netPrice = (TextView) findViewById(R.id.netPrice);
        total = (TextView) findViewById(R.id.total);
        orderTxt = (TextView) findViewById(R.id.orderTxt);
        main = (CoordinatorLayout) findViewById(R.id.main);
        checkout = (LinearLayout) findViewById(R.id.checkout);

        pintxt.setText(pin);
        mobileno.setText(phn);

        orderId = getIntent().getStringExtra("orderID");
        orderid.setText(orderId);

        totalVal = getIntent().getStringExtra("total");
        total.setText("Rs. "+totalVal);
        netPrice.setText("Rs. "+totalVal);

        pinStat = getIntent().getStringExtra("pinStat");
        if(pinStat.equalsIgnoreCase("false")){
            error.setVisibility(View.VISIBLE);
            error2.setVisibility(View.VISIBLE);
            orderTxt.setText("Booking can't be made");
            checkout.setClickable(false);
            checkout.setEnabled(false);
        }

        checkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("CLICK" ,"TRUE");
                Checkout();
            }
        });
    }

    private void Checkout(){

        pd = new ProgressDialog(ActivityCheckout.this,ProgressDialog.THEME_HOLO_DARK);
        pd.setMessage("Please Wait...");
        pd.setCancelable(false);
        pd.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.CHECKOUT,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Toast.makeText(mContext,response,Toast.LENGTH_LONG).show();
                        Log.e("response ** ",response);
                        pd.hide();
                        try {

                            JSONObject jsonObj = new JSONObject(response);
                            if(jsonObj.getString("status").equals("1")){
                                Snackbar snackbar = Snackbar
                                        .make(main, jsonObj.getString("message"), Snackbar.LENGTH_LONG);

                                snackbar.show();
                                Constants.CART_COUNT = 0;
                                Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        Intent i = new Intent(ActivityCheckout.this, MainActivity.class);
                                        startActivity(i);
                                        finish();
                                    }
                                },3000);
                            }else{
                                Snackbar snackbar = Snackbar
                                        .make(main, jsonObj.getString("message"), Snackbar.LENGTH_LONG);

                                snackbar.show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            pd.hide();
                            Snackbar snackbar = Snackbar
                                    .make(main, "Network error...", Snackbar.LENGTH_LONG);

                            snackbar.show();
                        }
                        //ProductAdapter adapter=new ProductAdapter(getActivity(), productArrayList);
                        //lv.setAdapter(adapter);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pd.hide();
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("orderID",orderId);
                params.put("cust_id",phn);
                Log.e("params==",""+params);
                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(ActivityCheckout.this);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,-1,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }
}
