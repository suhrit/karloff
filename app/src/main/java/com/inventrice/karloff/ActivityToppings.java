package com.inventrice.karloff;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.inventrice.karloff.Adapter.ViewPagerAdapterToppings;
import com.inventrice.karloff.CustomView.WrapContentHeightViewPager;
import com.inventrice.karloff.Model.ModelCategory;
import com.inventrice.karloff.Model.ModelSubCategory;
import com.inventrice.karloff.Model.ModelToppings;
import com.inventrice.karloff.Util.Constants;
import com.inventrice.karloff.database.DatabaseAdapter;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by suhrit on 10-09-2017.
 */

public class ActivityToppings extends AppCompatActivity {

    private TabLayout tabLayout;
    private WrapContentHeightViewPager viewPager;
    private AppBarLayout appbar;
    String isSubCat, order_id, toppings_desc,callfrom,product_id;
    ArrayList<ModelSubCategory> subCat = new ArrayList<ModelSubCategory>();
    CardView card_view3;
    static DatabaseAdapter dbHelper;
    CoordinatorLayout main;
    ProgressDialog pd;
    static String phn=null;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_toppings);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //What to do on back clicked
                finish();
            }
        });
        dbHelper = new DatabaseAdapter(ActivityToppings.this);
        dbHelper.open();

        SharedPreferences prefs = getSharedPreferences("login_pref", MODE_PRIVATE);
        phn = prefs.getString("phn", "NULL");

        main = (CoordinatorLayout) findViewById(R.id.main);
        card_view3 = (CardView) findViewById(R.id.card_view3);
        Log.e("callfrom",getIntent().getStringExtra("callfrom"));
        callfrom = getIntent().getStringExtra("callfrom");
        if(callfrom.equalsIgnoreCase("cartbtn")){
            card_view3.setVisibility(View.VISIBLE);
            toppings_desc = getIntent().getStringExtra("toppings_desc");
            if(getIntent().getStringExtra("type").equalsIgnoreCase("online")) {
                order_id = getIntent().getStringExtra("order_id");
            }else{
                card_view3.setVisibility(View.GONE);
            }
            product_id = getIntent().getStringExtra("product_id");
        }
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        viewPager = (WrapContentHeightViewPager) findViewById(R.id.view_pager);
        appbar = (AppBarLayout) findViewById(R.id.appbar);


        tabLayout.addTab(tabLayout.newTab().setText("Veg Toppings"));
        tabLayout.addTab(tabLayout.newTab().setText("Non-Veg Toppings"));
        final ViewPagerAdapterToppings adapter = new ViewPagerAdapterToppings
                (getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());


            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        card_view3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(dbHelper.getAddedToppingsByProduct(product_id).size()==0){
                    Snackbar snackbar = Snackbar
                            .make(main, "No Toppings Added Yet...", Snackbar.LENGTH_LONG);

                    snackbar.show();
                }else{
                    AddToppingsToCart();
                }
            }
        });

    }

    public void AddToppingsToCart(){

        pd = new ProgressDialog(ActivityToppings.this,ProgressDialog.THEME_HOLO_DARK);
        pd.setMessage("Please Wait...");
        pd.setCancelable(false);
        pd.show();

        ArrayList<ModelToppings> list = new ArrayList<ModelToppings>();
        list = dbHelper.getAddedToppingsByProduct(product_id);
        Log.e("toppings size ",""+list.size());
        Constants.CART_COUNT = Constants.CART_COUNT+list.size();
        Log.e("cart count",""+Constants.CART_COUNT);
        final ArrayList<ModelToppings> temp_list = list;

            for (int i = 0; i < list.size(); i++) {

                final int count = i + 1;


                final String topp_name = list.get(i).getName();
                final String topp_rate = list.get(i).getRate();
//Constants.ADDTOCART_TOPPINGS
                StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.ADDTOCART_TOPPINGS,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                //Toast.makeText(mContext,response,Toast.LENGTH_LONG).show();
                                pd.hide();
                                Log.e("response ** ", response);
                                try {
                                    JSONObject jsonObj = new JSONObject(response);
                                    if (jsonObj.getString("status").equals("1")) {
                                        Log.e("count ", "" + count);
                                        if (count == temp_list.size()) {
                                            Snackbar snackbar = Snackbar
                                                    .make(main, "Item Added To Cart", Snackbar.LENGTH_LONG);

                                            snackbar.show();

                                            dbHelper.deleteToppingsItemByProduct(product_id);

                                            ArrayList<ModelSubCategory> mscList = new ArrayList<ModelSubCategory>();
                                            ModelSubCategory msc1 = new ModelSubCategory();
                                            ModelSubCategory msc2 = new ModelSubCategory();
                                            ModelCategory mc = new ModelCategory();
                                            msc1.setSubCatID("2");
                                            msc2.setSubCatID("3");
                                            mscList.add(msc1);
                                            mscList.add(msc2);
                                            mc.setSubCatArray(mscList);


                                            Intent i1 = new Intent(ActivityToppings.this, ActivityMenuDetails.class);
                                            i1.putExtra("isSubCat","true");
                                            i1.putExtra("SubCat",(ArrayList<ModelSubCategory>)mc.getSubCatArray());
                                            startActivity(i1);
                                            finish();

                                        } else {
                                            Log.e("count ", "remain");
                                        }
                                    } else {

                                        //pd.hide();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    pd.hide();
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                //Toast.makeText(mContext,error.toString(),Toast.LENGTH_LONG).show();
                                pd.hide();
                            }
                        }) {
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("cust_id", phn);
                        params.put("order_id", order_id);
                        params.put("product_name", topp_name);
                        params.put("product_desc", toppings_desc);
                        params.put("rate", topp_rate);
                        params.put("full_amount", topp_rate);

                        Log.e("params==", "" + params);
                        return params;
                    }

                };

                RequestQueue requestQueue = Volley.newRequestQueue(ActivityToppings.this);
                stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,-1,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                requestQueue.add(stringRequest);
            }

    }
}
