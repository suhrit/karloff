package com.inventrice.karloff;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.SQLException;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.inventrice.karloff.Util.Constants;
import com.inventrice.karloff.database.DataBaseHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by suhrit on 26-08-2017.
 */

public class ActivitySplash extends Activity {
    String name = null,email=null, phn = null;
    boolean isLogin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_splash);

        SharedPreferences prefs = getSharedPreferences("login_pref", MODE_PRIVATE);


        name = prefs.getString("name", "NULL");//"No name defined" is the default value.
        email = prefs.getString("email", "NULL"); //0 is the default value.
        phn = prefs.getString("phn", "NULL");
        isLogin = prefs.getBoolean("isLogin", false);


        DataBaseHelper dbHelper = new DataBaseHelper(ActivitySplash.this);
        try {
            dbHelper.createDatabase();
            dbHelper.openDatabase();
            Log.e("TRY","TRUE");
        } catch (IOException e) {
            e.printStackTrace();
        } catch(SQLException sqle){

            throw sqle;
        }

        ProcessLogin();

        /*Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(isLogin == false) {
                    Intent i = new Intent(ActivitySplash.this, WelcomeActivity.class);
                    startActivity(i);
                    finish();
                }else{
                    Intent i = new Intent(ActivitySplash.this,MainActivity.class);
                    startActivity(i);
                    finish();
                }
            }
        },3000);*/
    }

    private void ProcessLogin(){

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.GET_CART_COUNT,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Toast.makeText(mContext,response,Toast.LENGTH_LONG).show();
                        Log.e("response ** ",response);
                        try {
                            JSONObject jsonObj = new JSONObject(response);

                                //Log.e("NAME",jsonObj.getString("name")+" "+jsonObj.getString("email"));

                                Constants.CART_COUNT = Integer.parseInt(jsonObj.getString("cartCount"));
                                Log.e("isLogin ",""+isLogin);
                                if(isLogin == false) {
                                    Intent i = new Intent(ActivitySplash.this, WelcomeActivity.class);
                                    startActivity(i);
                                    finish();
                                }else{
                                    Intent i = new Intent(ActivitySplash.this,MainActivity.class);
                                    startActivity(i);
                                    finish();
                                }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e("exception ",""+e);
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Toast.makeText(mContext,error.toString(),Toast.LENGTH_LONG).show();

                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("cust_id", phn);



                Log.e("params==",""+params);
                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(ActivitySplash.this);
        requestQueue.add(stringRequest);
    }
}
