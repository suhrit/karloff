package com.inventrice.karloff.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.inventrice.karloff.Model.ModelItemDetails;
import com.inventrice.karloff.Model.ModelOfflineCart;
import com.inventrice.karloff.Model.ModelTempToppingsStatus;
import com.inventrice.karloff.Model.ModelToppings;

import java.util.ArrayList;

/**
 * Created by suhrit on 02-07-2017.
 */

public class DatabaseAdapter {

    static final String TOPPINGS_INFO_TABLE_NAME="added_toppings";
    static final String ITEM_DETAILS_TABLE_NAME="Item_Details";
    static final String ITEM_IN_CART="cart";
    private Context context;
    private SQLiteDatabase database;
    private DataBaseHelper dbHelper;

    public DatabaseAdapter(Context context) {
        this.context = context;
    }

    public DatabaseAdapter open() throws SQLException {
        dbHelper = new DataBaseHelper(context);
        database = dbHelper.getWritableDatabase();



        return this;
    }

    public boolean InsertToppingsInfo(ModelTempToppingsStatus info) {

        boolean createSuccessful = false;

        ContentValues values = new ContentValues();

        //  values.put(KEY_ID, information.getId());


        values.put("product_id", ""+info.getProduct_id()+"");
        values.put("toppings_id", ""+info.getToppings_id()+"");
        values.put("toppings_name", info.getToppings_name());
        values.put("toppings_price", ""+info.getToppings_price()+"");
        values.put("cart_status", info.getCart_status());
        createSuccessful = database.insert(TOPPINGS_INFO_TABLE_NAME, null, values) > 0;
        //database.close();

        return createSuccessful;
    }

    public boolean checkToppingsStatus(String tpid, String pid) {

        boolean status= false;
        Cursor c = database.rawQuery("SELECT * FROM " + TOPPINGS_INFO_TABLE_NAME + " where product_id = "+pid+" AND toppings_id = "+tpid+"", null);
        if(c.getCount()>0){
            /*while (c.moveToNext()) {
                String train_no = c.getString(c.getColumnIndexOrThrow("toppings_id"));
            }*/
            status = true;
        }

        return status;

    }

    public boolean checkIfToppingsAdded(String pid) {

        boolean status= false;
        Cursor c = database.rawQuery("SELECT * FROM " + TOPPINGS_INFO_TABLE_NAME + " where product_id = "+pid+"", null);
        if(c.getCount()>0){
            /*while (c.moveToNext()) {
                String train_no = c.getString(c.getColumnIndexOrThrow("toppings_id"));
            }*/
            status = true;
        }

        return status;

    }

    public ArrayList<ModelToppings> getAddedToppingsByProduct(String pid){
        ArrayList<ModelToppings> list = new ArrayList<ModelToppings>();

        Cursor c = database.rawQuery("SELECT * FROM " + TOPPINGS_INFO_TABLE_NAME + " where product_id = "+pid+"", null);
        if(c.getCount()>0){
            while (c.moveToNext()) {
                ModelToppings mt = new ModelToppings();
                String name = c.getString(c.getColumnIndexOrThrow("toppings_name"));
                String price = c.getString(c.getColumnIndexOrThrow("toppings_price"));

                Log.e("tp name",name+" "+price);
                mt.setName(name);
                mt.setRate(price);
                list.add(mt);

            }

        }
        return list;
    }
    public double CalculateToppingsPrice(String pid) {

        double price= 0.00;
        Cursor c = database.rawQuery("SELECT * FROM " + TOPPINGS_INFO_TABLE_NAME + " where product_id = "+pid+"", null);
        if(c.getCount()>0){
            while (c.moveToNext()) {
                String toppings_price = c.getString(c.getColumnIndexOrThrow("toppings_price"));

                price = price+Double.valueOf(toppings_price);
                //Log.e("toppings_price ",""+price);
            }

        }

        return price;

    }

    public void deleteToppingsItem(String tpid, String pid){

        //Log.e("delete == ","DELETE FROM "+ TOPPINGS_INFO_TABLE_NAME + " WHERE product_id = "+pid+" AND toppings_id = "+tpid+"");
        database.execSQL("DELETE FROM "+ TOPPINGS_INFO_TABLE_NAME + " WHERE product_id = "+pid+" AND toppings_id = "+tpid+"");
        //database.close();
    }

    public void deleteToppingsItemByProduct(String pid){

        //Log.e("delete == ","DELETE FROM "+ TOPPINGS_INFO_TABLE_NAME + " WHERE product_id = "+pid+" AND toppings_id = "+tpid+"");
        database.execSQL("DELETE FROM "+ TOPPINGS_INFO_TABLE_NAME + " WHERE product_id = "+pid+"");
        //database.close();
    }

    public boolean InsertItemDetailsInfo(ModelItemDetails info) {

        boolean createSuccessful = false;

        ContentValues values = new ContentValues();

        //  values.put(KEY_ID, information.getId());


        values.put("product_id", info.getProduct_id());
        values.put("item_total", info.getItem_total());
        values.put("item_type", info.getItem_type());
        values.put("item_total_price", info.getItem_total_price());
        values.put("extra_cheese", String.valueOf(info.isExtra_cheese()));

        Cursor c = database.rawQuery("SELECT * FROM " + ITEM_DETAILS_TABLE_NAME + " where product_id = "+info.getProduct_id()+"", null);
        if(c.getCount()>0){
            Log.e("update","IF "+info.getProduct_id());
            database.update(ITEM_DETAILS_TABLE_NAME, values, "product_id="+info.getProduct_id(), null);
        }else {
            Log.e("insert","else "+info.getProduct_id());
            createSuccessful = database.insert(ITEM_DETAILS_TABLE_NAME, null, values) > 0;

        }

        return createSuccessful;
    }

    public ArrayList<ModelItemDetails> checkItemDetailsInfo( String pid) {

        Log.e("pid == ",""+pid);
        ArrayList<ModelItemDetails> list = new ArrayList<ModelItemDetails>();
        ModelItemDetails mid = new ModelItemDetails();
        //Log.e("queryy ","SELECT * FROM " + ITEM_DETAILS_TABLE_NAME + " where product_id = "+pid+"");
        Cursor c = database.rawQuery("SELECT * FROM " + ITEM_DETAILS_TABLE_NAME + " where product_id = "+pid+"", null);
        if(c.getCount()>0){
            Log.e("cursor count == ",""+c.getCount());
            /*while (c.moveToNext()) {
                String train_no = c.getString(c.getColumnIndexOrThrow("toppings_id"));
            }*/
            while (c.moveToNext()) {
                Log.e("get item total == ", "" + pid+" "+c.getString(c.getColumnIndexOrThrow("item_total")));
                mid.setProduct_id(c.getString(c.getColumnIndexOrThrow("product_id")));
                mid.setItem_type(c.getString(c.getColumnIndexOrThrow("item_type")));
                mid.setItem_total(c.getString(c.getColumnIndexOrThrow("item_total")));
                mid.setItem_total_price(c.getString(c.getColumnIndexOrThrow("item_total_price")));
                mid.setExtra_cheese(Boolean.valueOf(c.getString(c.getColumnIndexOrThrow("extra_cheese"))));

                list.add(mid);
            }
        }

        return list;

    }

    public boolean InsertItemToCart(ModelOfflineCart cart) {

        boolean createSuccessful = false;

        ContentValues values = new ContentValues();

        //  values.put(KEY_ID, information.getId());


        values.put("prod_id", cart.getId());
        values.put("product_name", cart.getName());
        values.put("product_desc", cart.getDesc());
        values.put("date", ""+cart.getDate());
        values.put("qty", cart.getQty());
        values.put("full_amount", cart.getPrice());
        values.put("extra_cheese", cart.getChzStat());
        values.put("rate", cart.getRate());
        values.put("ts", cart.getTime());
        values.put("extraChzPrice", cart.getExtrChzPrice());
        createSuccessful = database.insert(ITEM_IN_CART, null, values) > 0;
        //database.close();

        return createSuccessful;
    }

    public void DeleteItemDetails(){
        database.execSQL("DELETE FROM "+ ITEM_DETAILS_TABLE_NAME + "");
        database.execSQL("DELETE FROM "+ TOPPINGS_INFO_TABLE_NAME + "");
        database.execSQL("DELETE FROM "+ ITEM_IN_CART + "");
    }
}
