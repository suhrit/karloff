package com.inventrice.karloff;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.inventrice.karloff.Util.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by suhrit on 24-09-2017.
 */

public class AccountSettings extends AppCompatActivity {

    TextView phntxt,emailtxt;
    SharedPreferences prefs;
    String name,email,phn,landmark,h_no,st_name,pincode;
    EditText input_name,input_houseNo,input_streetName,input_landmark,input_pin;
    LinearLayout layout_submit;
    RelativeLayout content_account_settings;
    ProgressDialog pd;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_account_settings);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //What to do on back clicked
                finish();
            }
        });

        prefs = getSharedPreferences("login_pref", MODE_PRIVATE);


        name = prefs.getString("name", "NULL");//"No name defined" is the default value.
        email = prefs.getString("email", "NULL"); //0 is the default value.
        phn = prefs.getString("phn", "NULL");
        st_name = prefs.getString("st_name", "NULL");
        landmark = prefs.getString("landmark", "NULL");
        h_no = prefs.getString("h_no", "NULL");
        pincode = prefs.getString("pincode", "NULL");

        phntxt = (TextView) findViewById(R.id.phn);
        emailtxt = (TextView) findViewById(R.id.email);
        input_name = (EditText) findViewById(R.id.input_name);
        input_houseNo = (EditText) findViewById(R.id.input_houseNo);
        input_streetName = (EditText) findViewById(R.id.input_streetName);
        input_landmark = (EditText) findViewById(R.id.input_landmark);
        input_pin = (EditText) findViewById(R.id.input_pin);

        input_name.setText(name);
        input_houseNo.setText(h_no);
        input_streetName.setText(st_name);
        input_landmark.setText(landmark);
        input_pin.setText(pincode);


        phntxt.setText(phn);
        emailtxt.setText(email);

        content_account_settings = (RelativeLayout) findViewById(R.id.content_account_settings);
        layout_submit = (LinearLayout) findViewById(R.id.layout_submit);
        layout_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DoValidate();
            }
        });
    }

    private void DoValidate(){
        if(input_name.getText().toString().trim().isEmpty()){
            Snackbar snackbar = Snackbar
                    .make(content_account_settings, "Please enter your name", Snackbar.LENGTH_LONG);

            snackbar.show();
        }else if(input_houseNo.getText().toString().trim().isEmpty()){
            Snackbar snackbar = Snackbar
                    .make(content_account_settings, "Please enter house number", Snackbar.LENGTH_LONG);

            snackbar.show();
        }else if(input_streetName.getText().toString().trim().isEmpty()){
            Snackbar snackbar = Snackbar
                    .make(content_account_settings, "Please enter street name", Snackbar.LENGTH_LONG);

            snackbar.show();
        }else if(input_landmark.getText().toString().trim().isEmpty()){
            Snackbar snackbar = Snackbar
                    .make(content_account_settings, "Please enter landmark", Snackbar.LENGTH_LONG);

            snackbar.show();
        }else if(input_pin.getText().toString().trim().isEmpty()){
            Snackbar snackbar = Snackbar
                    .make(content_account_settings, "Please enter pin", Snackbar.LENGTH_LONG);

            snackbar.show();
        }else{
            ProcessRegister();
        }
    }

    private void ProcessRegister(){
        pd = new ProgressDialog(this,ProgressDialog.THEME_HOLO_DARK);
        pd.setMessage("Please Wait...");
        pd.setCancelable(false);
        pd.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.PROFILE_SETTINGS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Toast.makeText(mContext,response,Toast.LENGTH_LONG).show();
                        Log.e("response ** ",response);
                        try {
                            JSONObject jsonObj = new JSONObject(response);
                            if(jsonObj.getString("status").equals("1")){
                                /*Log.e("NAME",jsonObj.getString("name")+" "+jsonObj.getString("email"));
                                *//*ModelSession ses = new ModelSession(WelcomeActivity.this);
                                ses.setName(jsonObj.getString("name"),jsonObj.getString("email"));*/
                                SharedPreferences.Editor editor = getSharedPreferences("login_pref", MODE_PRIVATE).edit();
                                editor.putBoolean("isLogin",true);
                                editor.putString("name", jsonObj.getString("name"));
                                editor.putString("landmark", jsonObj.getString("landmark"));
                                editor.putString("h_no", jsonObj.getString("h_no"));
                                editor.putString("st_name", jsonObj.getString("st_name"));
                                editor.putString("pincode", jsonObj.getString("pincode"));
                                editor.apply();

                                pd.hide();

                                Intent i = new Intent(AccountSettings.this,ActivityProfile.class);
                                startActivity(i);
                                finish();
                            }else{
                                Snackbar snackbar = Snackbar
                                        .make(content_account_settings, "Account Not Found...", Snackbar.LENGTH_LONG);

                                snackbar.show();

                                pd.hide();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            pd.hide();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Toast.makeText(mContext,error.toString(),Toast.LENGTH_LONG).show();
                        pd.hide();
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("name", input_name.getText().toString().trim());
                params.put("phn", phn);
                params.put("h_no", input_houseNo.getText().toString().trim());
                params.put("st_name", input_streetName.getText().toString().trim());
                params.put("landmark", input_landmark.getText().toString().trim());
                params.put("pin", input_pin.getText().toString().trim());

                Log.e("params==",""+params);
                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(AccountSettings.this);
        requestQueue.add(stringRequest);
    }
}
