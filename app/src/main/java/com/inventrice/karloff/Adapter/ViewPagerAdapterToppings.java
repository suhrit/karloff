package com.inventrice.karloff.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

import com.inventrice.karloff.Fragment.NonVegToppingsFragment;
import com.inventrice.karloff.Fragment.VegToppingsFragment;

import java.util.ArrayList;

/**
 * Created by suhrit on 10-09-2017.
 */

public class ViewPagerAdapterToppings extends FragmentPagerAdapter {

    int mNumOfTabs;
    public ViewPagerAdapterToppings(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;

        ArrayList<Fragment> FRAGMENTS = new ArrayList<Fragment>();

        /*Fragment fragA = new LocalTrainFragment();
        LocalTrain.ISetTextInFragment setText = (LocalTrain.ISetTextInFragment) fragA;
        FRAGMENTS.add(fragA);*/
    }



    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:

                VegToppingsFragment tab1 = new VegToppingsFragment();
                Log.e("dfhd","0");
                return tab1;
            case 1:

                NonVegToppingsFragment tab2 = new NonVegToppingsFragment();
                Log.e("dfhd","1");
                return tab2;

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}

