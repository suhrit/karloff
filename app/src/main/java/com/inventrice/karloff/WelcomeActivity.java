package com.inventrice.karloff;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.inventrice.karloff.Model.ModelSession;
import com.inventrice.karloff.Util.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by suhrit on 28-08-2017.
 */

public class WelcomeActivity extends Activity implements View.OnClickListener{

    TextView skip;
    EditText pass,email;
    LinearLayout loginBtnLay,registerLay;
    ProgressDialog pd;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_welcome);

        skip = (TextView) findViewById(R.id.skip);
        email = (EditText) findViewById(R.id.email);
        pass = (EditText) findViewById(R.id.pass);

        loginBtnLay = (LinearLayout) findViewById(R.id.loginBtnLay);
        loginBtnLay.setOnClickListener(this);

        registerLay = (LinearLayout) findViewById(R.id.registerLay);
        registerLay.setOnClickListener(this);

        skip.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.loginBtnLay:
                ValidateLogin();
                break;

            case R.id.registerLay:
                MoveToRegisterPage();
                break;

            case R.id.skip:
                Intent i = new Intent(WelcomeActivity.this,MainActivity.class);
                startActivity(i);
                finish();
                break;
        }
    }

    public void ValidateLogin(){
        if(email.getText().toString().trim().isEmpty()){

        }else if(pass.getText().toString().trim().isEmpty()){

        }else{
            ProcessLogin();
        }
    }

    private void ProcessLogin(){
        pd = new ProgressDialog(this,ProgressDialog.THEME_HOLO_DARK);
        pd.setMessage("Please Wait...");
        pd.setCancelable(false);
        pd.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.LOGIN_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Toast.makeText(mContext,response,Toast.LENGTH_LONG).show();
                        Log.e("response ** ",response);
                        try {
                            JSONObject jsonObj = new JSONObject(response);
                            if(jsonObj.getString("status").equals("1")){
                                Log.e("NAME",jsonObj.getString("name")+" "+jsonObj.getString("email"));
                                /*ModelSession ses = new ModelSession(WelcomeActivity.this);
                                ses.setName(jsonObj.getString("name"),jsonObj.getString("email"));*/
                                Constants.CART_COUNT = Integer.parseInt(jsonObj.getString("cartCount"));

                                SharedPreferences.Editor editor = getSharedPreferences("login_pref", MODE_PRIVATE).edit();
                                editor.putBoolean("isLogin",true);
                                editor.putString("name", jsonObj.getString("name"));
                                editor.putString("email", jsonObj.getString("email"));
                                editor.putString("phn", jsonObj.getString("mobile"));
                                editor.putString("landmark", jsonObj.getString("landmark"));
                                editor.putString("h_no", jsonObj.getString("h_no"));
                                editor.putString("st_name", jsonObj.getString("st_name"));
                                editor.putString("pincode", jsonObj.getString("pincode"));


                                editor.apply();

                                pd.hide();

                                Intent i = new Intent(WelcomeActivity.this,MainActivity.class);
                                startActivity(i);
                                finish();
                            }else{
                                Toast.makeText(WelcomeActivity.this,"Account Not Found...",Toast.LENGTH_LONG).show();
                                pd.hide();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            pd.hide();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Toast.makeText(mContext,error.toString(),Toast.LENGTH_LONG).show();
                        pd.hide();
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("phone", email.getText().toString().trim());
                params.put("password", pass.getText().toString().trim());


                Log.e("params==",""+params);
                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(WelcomeActivity.this);
        requestQueue.add(stringRequest);
    }

    private void MoveToRegisterPage(){
        Intent i = new Intent(WelcomeActivity.this,ActivityRegister.class);
        startActivity(i);
    }
}
