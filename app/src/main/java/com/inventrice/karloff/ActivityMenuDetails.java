package com.inventrice.karloff;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.inventrice.karloff.Adapter.ViewPagerAdapter;
import com.inventrice.karloff.CustomView.WrapContentHeightViewPager;
import com.inventrice.karloff.Fragment.MenuTypeFragment1;
import com.inventrice.karloff.Fragment.MenuTypeFragment2;
import com.inventrice.karloff.Model.ModelSubCategory;
import com.inventrice.karloff.Util.NavigationDrawer;
import com.inventrice.karloff.database.DatabaseAdapter;

import java.util.ArrayList;

import static com.inventrice.karloff.ActivityCategoryOtherThanPizza.dbHelper;

/**
 * Created by inventrice on 02-Sep-17.
 */

public class ActivityMenuDetails extends NavigationDrawer {

    private TabLayout tabLayout;
    private WrapContentHeightViewPager viewPager;
    private AppBarLayout appbar;
    LinearLayout TabPrimaryLay;
    String isSubCat;
    static DatabaseAdapter dbHelper;
    ArrayList<ModelSubCategory> subCat = new ArrayList<ModelSubCategory>();
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_menu_details);
        super.init();
        isSubCat = getIntent().getStringExtra("isSubCat");
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        viewPager = (WrapContentHeightViewPager) findViewById(R.id.view_pager);
        appbar = (AppBarLayout) findViewById(R.id.appbar);
        TabPrimaryLay = (LinearLayout) findViewById(R.id.TabPrimaryLay);

        dbHelper = new DatabaseAdapter(ActivityMenuDetails.this);
        dbHelper.open();

        if(isSubCat.equals("true")) {
            subCat = (ArrayList<ModelSubCategory>) getIntent().getSerializableExtra("SubCat");
            //List<String> myList = new ArrayList<String>(Arrays.asList(subCat));
            Log.e("myList size ", "" + subCat.size());
            Log.e("CATID ", "" + subCat.get(0).getSubCatID());
            Log.e("CATID2 ", "" + subCat.get(1).getSubCatID());
            MenuTypeFragment1.catID = subCat.get(0).getSubCatID();
            MenuTypeFragment2.catID = subCat.get(1).getSubCatID();
        }
            tabLayout.addTab(tabLayout.newTab().setText("Veg Pizza"));
            tabLayout.addTab(tabLayout.newTab().setText("Non-Veg Pizza"));
            final ViewPagerAdapter adapter = new ViewPagerAdapter
                    (getSupportFragmentManager(), tabLayout.getTabCount());
            viewPager.setAdapter(adapter);
            viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

            tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(TabLayout.Tab tab) {
                    viewPager.setCurrentItem(tab.getPosition());


                }

                @Override
                public void onTabUnselected(TabLayout.Tab tab) {

                }

                @Override
                public void onTabReselected(TabLayout.Tab tab) {

                }
            });


        /*if(isSubCat.equals("false")){
            tabLayout.setVisibility(View.GONE);
            tabLayout.removeTab(tabLayout.getTabAt(1));
            viewPager.setPagingEnabled(false);
            TabPrimaryLay.setVisibility(View.GONE);
        }else {
        }*/
    }
    @Override
    public void onBackPressed(){
        super.onBackPressed();
        dbHelper.DeleteItemDetails();
    }

}
