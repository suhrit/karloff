package com.inventrice.karloff;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.inventrice.karloff.Model.ModelCategory;
import com.inventrice.karloff.Model.ModelSubCategory;
import com.inventrice.karloff.Util.Constants;
import com.inventrice.karloff.Util.NavigationDrawer;
import com.inventrice.karloff.Util.PicassoLoader;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cn.lightsky.infiniteindicator.IndicatorConfiguration;
import cn.lightsky.infiniteindicator.InfiniteIndicator;
import cn.lightsky.infiniteindicator.OnPageClickListener;
import cn.lightsky.infiniteindicator.Page;


/**
 * Created by matainja on 01-Sep-17.
 */

public class ActivityMenu extends NavigationDrawer implements ViewPager.OnPageChangeListener,OnPageClickListener{
    ListView catlist;
    private ArrayList<Page> pageViews;
    private InfiniteIndicator mAnimLineIndicator;
    ProgressBar progressBar;
    ArrayList<ModelCategory> catArrayList;
    CategoryListAdapter CLA;
    public static boolean isVisible = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_menu);
        super.init();

        progressBar = (ProgressBar) findViewById(R.id.progressBar2);
        catlist = (ListView) findViewById(R.id.catlist);
        FetchCategory();

        initData();
        testAnimLineIndicator();

    }

    private void initData() {
        pageViews = new ArrayList<>();

        pageViews.add(new Page("A", R.drawable.cat_pizza, this));
        pageViews.add(new Page("B", R.drawable.cat_combo_meal, this));
        pageViews.add(new Page("C", R.drawable.cat_fried_chicken, this));


    }

    private void testAnimLineIndicator() {
        mAnimLineIndicator = (InfiniteIndicator) findViewById(R.id.infinite_anim_line);
        IndicatorConfiguration configuration = new IndicatorConfiguration.Builder()
                .imageLoader(new PicassoLoader())
                .isAutoScroll(true)
                .isStopWhileTouch(true)
                .onPageChangeListener(this)
                .position(IndicatorConfiguration.IndicatorPosition.Center_Bottom)
                .build();
        mAnimLineIndicator.init(configuration);
        mAnimLineIndicator.notifyDataChange(pageViews);
    }
    private void FetchCategory(){

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.CATEGORY_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Toast.makeText(mContext,response,Toast.LENGTH_LONG).show();
                        Log.e("response ** ",response);
                        catArrayList = new ArrayList<ModelCategory>();
                        try {
                            JSONObject jsonObj = new JSONObject(response);
                            if(jsonObj.getString("status").equals("1")){
                                for(int i = 0; i < jsonObj.getJSONArray("result").length();i++){
                                    ModelCategory mc = new ModelCategory();
                                    JSONObject js = jsonObj.getJSONArray("result").getJSONObject(i);
                                    mc.setCategoryID(js.getString("categoryID"));
                                    mc.setParentCategoryID(js.getString("parentcategoryID"));
                                    mc.setCategoryName(js.getString("categoryName"));
                                    mc.setCategoryImage(js.getString("categoryImage"));
                                    mc.setSubCat(js.getBoolean("isSubCat"));

                                    JSONArray subCat = js.getJSONArray("sub_category");
                                    ArrayList<ModelSubCategory> subCatArray = new ArrayList<ModelSubCategory>();
                                    if(subCat.length()>0){
                                        for(int j =0;j<subCat.length();j++){
                                            JSONObject subCatOBJ = subCat.getJSONObject(j);
                                            ModelSubCategory msc = new ModelSubCategory();
                                            msc.setSubCatID(subCatOBJ.getString("categoryID"));
                                            msc.setSubCatName(subCatOBJ.getString("categoryName"));

                                            subCatArray.add(msc);
                                            mc.setSubCatArray(subCatArray);
                                        }
                                    }
                                    catArrayList.add(mc);
                                }
                                CLA = new CategoryListAdapter(ActivityMenu.this, catArrayList);
                                catlist.setAdapter(CLA);
                                progressBar.setVisibility(View.GONE);
                            }else{
                                progressBar.setVisibility(View.GONE);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressBar.setVisibility(View.GONE);
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Toast.makeText(mContext,error.toString(),Toast.LENGTH_LONG).show();
                        progressBar.setVisibility(View.GONE);
                    }
                }){
            /*@Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("email", email.getText().toString().trim());
                params.put("password", pass.getText().toString().trim());


                Log.e("params==",""+params);
                return params;
            }*/

        };

        RequestQueue requestQueue = Volley.newRequestQueue(ActivityMenu.this);
        requestQueue.add(stringRequest);
    }


    @Override
    public void onPageClick(int position, Page page) {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    public class CategoryListAdapter extends BaseAdapter{

        Context context;
        private LayoutInflater inflater;
        ViewHolder holder = null;
        ArrayList<ModelCategory> catList;
        public CategoryListAdapter(Context context,ArrayList<ModelCategory> catList){
            this.context = context;
            this.catList = catList;
        }
        @Override
        public int getCount() {
            return catList.size();
        }

        @Override
        public Object getItem(int position) {
            return catList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        private class ViewHolder {
            TextView Txt1, Txt2, Txt3,categoryName;
            ImageView Img1, Img2, Img3;
            LinearLayout l1, l2, l3;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.row_category_list, parent, false);//layout
                holder = new ViewHolder();



                /*holder.categoryName = (TextView) convertView.findViewById(R.id.categoryName);*/
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            holder.Txt1 = (TextView) convertView.findViewById(R.id.txt1);
            holder.Txt2 = (TextView) convertView.findViewById(R.id.txt2);
            holder.Txt3 = (TextView) convertView.findViewById(R.id.txt21);
            holder.Img1 = (ImageView) convertView.findViewById(R.id.img1);
            holder.Img2 = (ImageView) convertView.findViewById(R.id.img2);
            holder.Img3 = (ImageView) convertView.findViewById(R.id.img21);
            holder.l1 = (LinearLayout) convertView.findViewById(R.id.lay1);
            holder.l2 = (LinearLayout) convertView.findViewById(R.id.lay2);
            holder.l3 = (LinearLayout) convertView.findViewById(R.id.lay3);

            /*holder.categoryName.setText(Items.get(position).get(Constants.TAG_CATEGORYNAME));*/

            /*convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.e("ITEM NAME== ",Items.get(position).get(Constants.TAG_CATEGORYNAME));

                    FragmentMainActivity.filterAry.clear();
                    SearchAdvance.applyFilter = true;
                    SearchAdvance.distance = "0";
                    SearchAdvance.categoryId.clear();
                    SearchAdvance.categoryName.clear();
                    SearchAdvance.subcategoryId.clear();
                    SearchAdvance.postedWithin = "";
                    SearchAdvance.sortBy = "1";
                    SearchAdvance.categoryId.add(Items.get(position).get(Constants.TAG_CATEGORYID));
                    SearchAdvance.categoryName.add(Items.get(position).get(Constants.TAG_CATEGORYNAME));
                    finish();
                    Intent i = new Intent(CategoryActivity.this, FragmentMainActivity.class);
                    startActivity(i);

                }
            });*/
            try {
                holder.l1.setVisibility(View.GONE);
                holder.l2.setVisibility(View.GONE);
                holder.l3.setVisibility(View.GONE);
                //final HashMap<String, String> tempMap = catList.get(position);
                switch (position % 2) {
                    case 0:
                        if (catList.size() % 2 != 0 && (position == getCount())) {

                            Log.e("CASE 0 ","IF");
                            holder.l1.setVisibility(View.GONE);
                            holder.l2.setVisibility(View.GONE);
                            holder.l3.setVisibility(View.VISIBLE);
                            holder.Txt3.setText(catList.get(position).getCategoryName());

                            Picasso.with(ActivityMenu.this).load(Constants.BASE_URL+"/"+catList.get(position).getCategoryImage()).into(holder.Img3);

                        } else {
                            Log.e("CASE 0 ","ELSE");
                            holder.l1.setVisibility(View.VISIBLE);
                            holder.l2.setVisibility(View.VISIBLE);
                            holder.l3.setVisibility(View.GONE);

                            holder.Txt1.setText(catList.get(position).getCategoryName());

                            Picasso.with(ActivityMenu.this).load(Constants.BASE_URL+"/"+catList.get(position).getCategoryImage()).into(holder.Img1);

                            holder.Txt2.setText(catList.get(position + 1).getCategoryName());
                            Picasso.with(ActivityMenu.this).load(Constants.BASE_URL+"/"+catList.get(position + 1).getCategoryImage()).into(holder.Img2);
                        }
                        break;
                    case 2:
                        Log.e("CASE 2 ","TRUE");
                        break;
                }

                holder.l1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //FragmentMainActivity.filterAry.clear();
                        /*SearchAdvance.applyFilter = true;
                        SearchAdvance.distance = "0";
                        SearchAdvance.categoryId.clear();
                        SearchAdvance.categoryName.clear();
                        SearchAdvance.subcategoryId.clear();
                        SearchAdvance.postedWithin = "";
                        SearchAdvance.sortBy = "1";
                        SearchAdvance.categoryId.add(Items.get(position).get(Constants.TAG_CATEGORYID));
                        SearchAdvance.categoryName.add(Items.get(position).get(Constants.TAG_CATEGORYNAME));*/
                        //finish();
                        Log.e("isSubCat ","lay2 "+catList.get(position).isSubCat()+" "+catList.get(position).getCategoryName()+" "+position);
                        Intent i = new Intent(ActivityMenu.this, ActivityMenuDetails.class);
                        i.putExtra("isSubCat",String.valueOf(catList.get(position).isSubCat()));
                        if(catList.get(position).isSubCat() == true) {
                            i.putExtra("SubCat", (ArrayList<ModelSubCategory>)catList.get(position).getSubCatArray());
                            //i.putExtra("subCatID", catList.get(position).getSubCatArray().get(0).getSubCatID());
                        }else{
                            i.putExtra("CatName", catList.get(position).getCategoryName());
                            i.putExtra("CatID", catList.get(position).getCategoryID());
                        }
                        i.putExtra("isSubCat",String.valueOf(catList.get(position).isSubCat()));
                        startActivity(i);
                    }
                });
                holder.l2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.e("isSubCat ","lay2 "+catList.get(position+1).isSubCat()+" "+catList.get(position).getCategoryName()+" "+position);

                        Intent i = new Intent(ActivityMenu.this, ActivityCategoryOtherThanPizza.class);
                        i.putExtra("isSubCat",String.valueOf(catList.get(position+1).isSubCat()));
                        if(catList.get(position+1).isSubCat() == true) {
                            i.putExtra("SubCat", (ArrayList<ModelSubCategory>)catList.get(position+1).getSubCatArray());
                            //i.putExtra("CatID", catList.get(position+1).getSubCatArray().get(0).getSubCatID());
                            Log.e("IF ","TRUE");
                        }else{
                            Log.e("ELSE ","TRUE");
                            i.putExtra("CatName", catList.get(position+1).getCategoryName());
                            i.putExtra("CatID", catList.get(position+1).getCategoryID());
                        }
                        startActivity(i);
                    }
                });
                holder.l3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.e("isSubCat ","lay2 "+catList.get(catList.size()-1).isSubCat()+" "+catList.get(catList.size()-1).getCategoryName()+" "+position);
                        Intent i = new Intent(ActivityMenu.this, ActivityCategoryOtherThanPizza.class);
                        i.putExtra("isSubCat",String.valueOf(catList.get(catList.size()-1).isSubCat()));
                        if(catList.get(catList.size()-1).isSubCat() == true) {
                            i.putExtra("SubCat", (ArrayList<ModelSubCategory>)catList.get(catList.size()-1).getSubCatArray());
                            //i.putExtra("CatID", catList.get(catList.size()-1).getSubCatArray().get(0).getSubCatID());
                        }else{
                            i.putExtra("CatName", catList.get(catList.size()-1).getCategoryName());
                            i.putExtra("CatID", catList.get(catList.size()-1).getCategoryID());
                        }
                        startActivity(i);
                    }
                });

            } catch (NullPointerException e) {
                e.printStackTrace();
            } catch (Exception e) {
                holder.l1.setVisibility(View.GONE);
                holder.l2.setVisibility(View.GONE);
                holder.l3.setVisibility(View.VISIBLE);
                holder.Txt3.setText(catList.get(position).getCategoryName());
                Picasso.with(ActivityMenu.this).load(Constants.BASE_URL+"/"+catList.get(position).getCategoryImage()).into(holder.Img3);
                holder.l3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(ActivityMenu.this, ActivityCategoryOtherThanPizza.class);
                        i.putExtra("isSubCat",String.valueOf(catList.get(position).isSubCat()));
                        if(catList.get(position).isSubCat() == true) {
                            i.putExtra("SubCat", (ArrayList<ModelSubCategory>)catList.get(position).getSubCatArray());
                            //i.putExtra("CatID", catList.get(position).getSubCatID());
                        }else{
                            i.putExtra("CatName", catList.get(position).getCategoryName());
                            i.putExtra("CatID", catList.get(position).getCategoryID());
                        }
                        startActivity(i);
                        Log.e("isSubCat ",""+catList.get(position).isSubCat()+" "+catList.get(position).getCategoryName()+" "+position);
                    }
                });
                e.printStackTrace();
            }
            return convertView;
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        isVisible = true;

        cart_count_lay = (LinearLayout) findViewById(R.id.cart_count_lay);
        cartCount = (TextView) findViewById(R.id.cartCount);

        if(Constants.CART_COUNT == 0){
            cart_count_lay.setVisibility(View.GONE);
        }else {
            cartCount.setText(String.valueOf(Constants.CART_COUNT));
            cart_count_lay.setVisibility(View.VISIBLE);
        }

    }
    @Override
    public void onStop(){
        super.onStop();
        isVisible = false;
    }
}
