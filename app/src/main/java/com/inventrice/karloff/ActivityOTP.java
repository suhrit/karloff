package com.inventrice.karloff;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.inventrice.karloff.Util.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by suhrit on 24-10-2017.
 */

public class ActivityOTP extends AppCompatActivity {

    EditText input_otp;
    String name,email,pass,phn,h_no,st_name,landmark,pin,otp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_otp);
        /*Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //What to do on back clicked
                finish();
            }
        });*/

        TextInputLayout input_layout_otp = (TextInputLayout) findViewById(R.id.input_layout_otp);
        input_otp = (EditText) findViewById(R.id.input_otp);
        LinearLayout submit = (LinearLayout) findViewById(R.id.submit);

        name = getIntent().getStringExtra("name");
        email = getIntent().getStringExtra("email");
        pass = getIntent().getStringExtra("pass");
        phn = getIntent().getStringExtra("phn");
        h_no = getIntent().getStringExtra("h_no");
        st_name = getIntent().getStringExtra("st_name");
        landmark = getIntent().getStringExtra("landmark");
        pin = getIntent().getStringExtra("pin");
        otp = getIntent().getStringExtra("otp");

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(otp.equalsIgnoreCase(input_otp.getText().toString().trim())) {
                    ProcessRegister();
                }else{
                    Toast.makeText(ActivityOTP.this,"OTP not matching",Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void ProcessRegister(){
        final ProgressDialog pd = new ProgressDialog(this,ProgressDialog.THEME_HOLO_DARK);
        pd.setMessage("Please Wait...");
        pd.setCancelable(false);
        pd.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.REGISTER_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Toast.makeText(mContext,response,Toast.LENGTH_LONG).show();
                        Log.e("response ** ",response);
                        try {
                            JSONObject jsonObj = new JSONObject(response);
                            if(jsonObj.getString("status").equals("1")){
                                /*Log.e("NAME",jsonObj.getString("name")+" "+jsonObj.getString("email"));
                                *//*ModelSession ses = new ModelSession(WelcomeActivity.this);
                                ses.setName(jsonObj.getString("name"),jsonObj.getString("email"));*/


                                SharedPreferences.Editor editor = getSharedPreferences("login_pref", MODE_PRIVATE).edit();
                                editor.putBoolean("isLogin",true);
                                editor.putString("name", jsonObj.getString("name"));
                                editor.putString("email", jsonObj.getString("email"));
                                editor.putString("phn", jsonObj.getString("mobile"));
                                editor.putString("landmark", jsonObj.getString("landmark"));
                                editor.putString("h_no", jsonObj.getString("h_no"));
                                editor.putString("st_name", jsonObj.getString("st_name"));
                                editor.putString("pincode", jsonObj.getString("pincode"));
                                editor.apply();

                                pd.hide();
                                Intent i = new Intent(ActivityOTP.this,MainActivity.class);
                                startActivity(i);
                                finish();
                            }else{
                                Toast.makeText(ActivityOTP.this,"Account Not Found...",Toast.LENGTH_LONG).show();
                                pd.hide();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            pd.hide();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Toast.makeText(mContext,error.toString(),Toast.LENGTH_LONG).show();
                        pd.hide();
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("name", name);
                params.put("email",email );
                params.put("pass",pass);
                params.put("phn", phn);
                params.put("h_no", h_no);
                params.put("st_name", st_name);
                params.put("landmark",landmark);
                params.put("pin", pin);
                params.put("OTP","false");
                Log.e("params==",""+params);
                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(ActivityOTP.this);
        requestQueue.add(stringRequest);
    }
}
