package com.inventrice.karloff;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.TextView;

import com.inventrice.karloff.Util.NavigationDrawer;

/**
 * Created by suhrit on 23-09-2017.
 */

public class ActivityProfile extends NavigationDrawer {

    SharedPreferences prefs;
    TextView nametxt,emailtxt,phntxt,initialtxt;
    CardView account_settings_card,logout_card,order_card;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_profile);
        super.init();

        prefs = getSharedPreferences("login_pref", MODE_PRIVATE);


        name = prefs.getString("name", "NULL");//"No name defined" is the default value.
        email = prefs.getString("email", "NULL");
        phn = prefs.getString("phn", "NULL");

        nametxt = (TextView) findViewById(R.id.name);
        initialtxt = (TextView) findViewById(R.id.initial);
        emailtxt = (TextView) findViewById(R.id.email);
        phntxt = (TextView) findViewById(R.id.phn);

        nametxt.setText(name);
        emailtxt.setText(email);
        phntxt.setText(phn);

        char initial = name.charAt(0);
        initialtxt.setText(String.valueOf(initial).toUpperCase());
        initialtxt.setTextSize(50);

        order_card = (CardView) findViewById(R.id.order_card);
        order_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ActivityProfile.this,AccountOrders.class);
                startActivity(i);
            }
        });
        account_settings_card = (CardView) findViewById(R.id.account_settings_card);
        account_settings_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ActivityProfile.this,AccountSettings.class);
                startActivity(i);
            }
        });

        logout_card = (CardView) findViewById(R.id.logout_card);
        logout_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which){
                            case DialogInterface.BUTTON_POSITIVE:
                                //Yes button clicked
                                SharedPreferences.Editor editor = getSharedPreferences("login_pref", MODE_PRIVATE).edit();
                                editor.clear().commit();

                                Intent i = new Intent(ActivityProfile.this,MainActivity.class);
                                startActivity(i);
                                finish();
                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                //No button clicked
                                break;
                        }
                    }
                };

                AlertDialog.Builder builder = new AlertDialog.Builder(ActivityProfile.this);
                builder.setMessage("Do you want logout from app?").setPositiveButton("Yes", dialogClickListener)
                        .setNegativeButton("No", dialogClickListener).show();

            }
        });
    }
}
