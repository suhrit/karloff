package com.inventrice.karloff;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.inventrice.karloff.Util.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * Created by matainja on 30-Aug-17.
 */

public class ActivityRegister extends AppCompatActivity implements View.OnClickListener{

    EditText input_phn,input_password,input_cnfpassword,input_name,input_email,input_houseNo,
            input_streetName,input_landmark,input_pin,input_otp;
    LinearLayout RegisterBtn;
    RelativeLayout main;
    ProgressDialog pd;
    //TextView otpBtn;
    //LinearLayout sendOtpLay;
    //TextInputLayout input_layout_otp;
    String otp;
    ImageView back;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_register);

        input_phn = (EditText) findViewById(R.id.input_phn);
        input_password = (EditText) findViewById(R.id.input_password);
        input_cnfpassword = (EditText) findViewById(R.id.input_cnfpassword);
        input_name = (EditText) findViewById(R.id.input_name);
        input_email = (EditText) findViewById(R.id.input_email);
        input_houseNo = (EditText) findViewById(R.id.input_houseNo);
        input_streetName = (EditText) findViewById(R.id.input_streetName);
        input_landmark = (EditText) findViewById(R.id.input_landmark);
        input_pin = (EditText) findViewById(R.id.input_pin);
        input_otp = (EditText) findViewById(R.id.input_otp);



        RegisterBtn = (LinearLayout) findViewById(R.id.RegisterBtn);
        RegisterBtn.setOnClickListener(this);

        main = (RelativeLayout) findViewById(R.id.main);



        back = (ImageView) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    public void SendOTP(final String phn){
        pd = new ProgressDialog(ActivityRegister.this,ProgressDialog.THEME_HOLO_DARK);
        pd.setMessage("Please Wait...Sending OTP...");
        pd.setCancelable(false);
        pd.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.REGISTER_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Toast.makeText(mContext,response,Toast.LENGTH_LONG).show();
                        Log.e("response ** ",response);

                        try {

                            JSONObject jsonObj = new JSONObject(response);
                            if(jsonObj.getString("status").equals("1")) {

                                otp = jsonObj.getString("otp");
                                Log.e("otp ",otp);

                                Intent i =new Intent(ActivityRegister.this,ActivityOTP.class);
                                i.putExtra("name ",input_name.getText().toString().trim());
                                i.putExtra("email ",input_email.getText().toString().trim());
                                i.putExtra("pass", input_password.getText().toString().trim());
                                i.putExtra("phn", input_phn.getText().toString().trim());
                                i.putExtra("h_no", input_houseNo.getText().toString().trim());
                                i.putExtra("st_name", input_streetName.getText().toString().trim());
                                i.putExtra("landmark", input_landmark.getText().toString().trim());
                                i.putExtra("pin", input_pin.getText().toString().trim());
                                i.putExtra("otp", otp);
                                startActivity(i);
                                finish();
                                /*Snackbar snackbar = Snackbar
                                        .make(main, jsonObj.getString("message"), Snackbar.LENGTH_LONG);

                                snackbar.show();*/
                                pd.hide();

                            }else{
                                Snackbar snackbar = Snackbar
                                        .make(main, jsonObj.getString("message"), Snackbar.LENGTH_LONG);

                                snackbar.show();
                                pd.hide();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            pd.hide();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Toast.makeText(mContext,error.toString(),Toast.LENGTH_LONG).show();
                        pd.hide();
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("phn",phn);
                params.put("OTP","true");
                Log.e("params==",""+params);
                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(ActivityRegister.this);
        requestQueue.add(stringRequest);
    }
    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.RegisterBtn:

                DoValidate();
                break;
        }
    }

    private void DoValidate(){
        if(!input_email.getText().toString().trim().isEmpty()){
            if(!isValidMail(input_email.getText().toString().trim())){
                Snackbar snackbar = Snackbar
                        .make(main, "Please enter a valid email", Snackbar.LENGTH_LONG);

                snackbar.show();
            }else{

            }
        }
        if(input_phn.getText().toString().trim().isEmpty()){
            Snackbar snackbar = Snackbar
                    .make(main, "Please enter phone number", Snackbar.LENGTH_LONG);

            snackbar.show();
        }else if(!validCellPhone(input_phn.getText().toString().trim())){
            Snackbar snackbar = Snackbar
                    .make(main, "Please enter a valid phone number", Snackbar.LENGTH_LONG);

            snackbar.show();
        }else if(input_password.getText().toString().trim().isEmpty() || input_cnfpassword.getText().toString().trim().isEmpty()){
            Snackbar snackbar = Snackbar
                    .make(main, "Please enter password", Snackbar.LENGTH_LONG);

            snackbar.show();
        }else if(!input_password.getText().toString().trim().equals(input_cnfpassword.getText().toString().trim())){
            Snackbar snackbar = Snackbar
                    .make(main, "Password not match", Snackbar.LENGTH_LONG);

            snackbar.show();
        }else if(input_name.getText().toString().trim().isEmpty()){
            Snackbar snackbar = Snackbar
                    .make(main, "Please enter your name", Snackbar.LENGTH_LONG);

            snackbar.show();
        }else if(input_houseNo.getText().toString().trim().isEmpty()){
            Snackbar snackbar = Snackbar
                    .make(main, "Please enter house number", Snackbar.LENGTH_LONG);

            snackbar.show();
        }else if(input_streetName.getText().toString().trim().isEmpty()){
            Snackbar snackbar = Snackbar
                    .make(main, "Please enter street name", Snackbar.LENGTH_LONG);

            snackbar.show();
        }else if(input_landmark.getText().toString().trim().isEmpty()){
            Snackbar snackbar = Snackbar
                    .make(main, "Please enter landmark", Snackbar.LENGTH_LONG);

            snackbar.show();
        }else if(input_pin.getText().toString().trim().isEmpty()){
            Snackbar snackbar = Snackbar
                    .make(main, "Please enter pin", Snackbar.LENGTH_LONG);

            snackbar.show();
        }else if(input_otp.getText().toString().isEmpty()){
            Snackbar snackbar = Snackbar
                    .make(main, "Please enter OTP", Snackbar.LENGTH_LONG);

            snackbar.show();
        }else if(!otp.equalsIgnoreCase(input_otp.getText().toString())){
            Snackbar snackbar = Snackbar
                    .make(main, "Please enter valid OTP", Snackbar.LENGTH_LONG);

            snackbar.show();
        }else{
            //ProcessRegister();
            SendOTP(input_phn.getText().toString().trim());
        }
    }

    public boolean validCellPhone(String number)
    {
        //return android.util.Patterns.PHONE.matcher(number).matches();
        boolean check=false;
        if(!Pattern.matches("[a-zA-Z]+", number)) {
            if(number.length() < 8 || number.length() > 10) {
                // if(phone.length() != 10) {
                check = false;
                //txtPhone.setError("Not Valid Number");
            } else {
                check = true;
            }
        } else {
            check=false;
        }
        return check;
    }
    private boolean isValidMail(String email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }


}
