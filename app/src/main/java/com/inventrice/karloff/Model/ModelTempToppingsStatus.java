package com.inventrice.karloff.Model;

/**
 * Created by matainja on 16-Sep-17.
 */

public class ModelTempToppingsStatus {

    String product_id, toppings_id, toppings_name, toppings_price,cart_status;

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getToppings_id() {
        return toppings_id;
    }

    public void setToppings_id(String toppings_id) {
        this.toppings_id = toppings_id;
    }

    public String getToppings_name() {
        return toppings_name;
    }

    public void setToppings_name(String toppings_name) {
        this.toppings_name = toppings_name;
    }

    public String getToppings_price() {
        return toppings_price;
    }

    public void setToppings_price(String toppings_price) {
        this.toppings_price = toppings_price;
    }

    public String getCart_status() {
        return cart_status;
    }

    public void setCart_status(String cart_status) {
        this.cart_status = cart_status;
    }
}
