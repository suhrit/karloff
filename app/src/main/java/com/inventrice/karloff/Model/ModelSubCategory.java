package com.inventrice.karloff.Model;

import java.io.Serializable;

/**
 * Created by suhrit on 02-09-2017.
 */

public class ModelSubCategory implements Serializable {
    String subCatName,subCatID;

    public String getSubCatName() {
        return subCatName;
    }

    public void setSubCatName(String subCatName) {
        this.subCatName = subCatName;
    }

    public String getSubCatID() {
        return subCatID;
    }

    public void setSubCatID(String subCatID) {
        this.subCatID = subCatID;
    }
}
