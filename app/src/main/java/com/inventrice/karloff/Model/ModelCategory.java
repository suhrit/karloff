package com.inventrice.karloff.Model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by matainja on 01-Sep-17.
 */

public class ModelCategory implements Serializable {
    String categoryID,parentCategoryID,categoryName,categoryImage;
    boolean isSubCat;
    ArrayList<ModelSubCategory> subCatArray;

    public String getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(String categoryID) {
        this.categoryID = categoryID;
    }

    public String getParentCategoryID() {
        return parentCategoryID;
    }

    public void setParentCategoryID(String parentCategoryID) {
        this.parentCategoryID = parentCategoryID;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryImage() {
        return categoryImage;
    }

    public void setCategoryImage(String categoryImage) {
        this.categoryImage = categoryImage;
    }

    public boolean isSubCat() {
        return isSubCat;
    }

    public void setSubCat(boolean subCat) {
        isSubCat = subCat;
    }

    public ArrayList<ModelSubCategory> getSubCatArray() {
        return subCatArray;
    }

    public void setSubCatArray(ArrayList<ModelSubCategory> subCatArray) {
        this.subCatArray = subCatArray;
    }
}
