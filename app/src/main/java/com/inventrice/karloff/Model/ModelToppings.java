package com.inventrice.karloff.Model;

/**
 * Created by suhrit on 10-09-2017.
 */

public class ModelToppings {
    String id,name,tp_type,img_path,rate;
    boolean checked;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTp_type() {
        return tp_type;
    }

    public void setTp_type(String tp_type) {
        this.tp_type = tp_type;
    }

    public String getImg_path() {
        return img_path;
    }

    public void setImg_path(String img_path) {
        this.img_path = img_path;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }
}
