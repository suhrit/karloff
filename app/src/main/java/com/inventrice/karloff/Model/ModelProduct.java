package com.inventrice.karloff.Model;

/**
 * Created by matainja on 07-Sep-17.
 */

public class ModelProduct {
    String name;
    String id;
    String image;
    String desc;
    String medium_price;
    String larger_price;
    String regular_price;
    String extrChzPrice;


    public String getExtrChzPrice() {
        return extrChzPrice;
    }

    public void setExtrChzPrice(String extrChzPrice) {
        this.extrChzPrice = extrChzPrice;
    }

    public String getOrder_id() {
        return order_id;
    }
    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    String order_id;
    int count,price;
    boolean isAddToCart;
    double cal_toppings_price;

    public double getCal_toppings_price() {
        return cal_toppings_price;
    }

    public void setCal_toppings_price(double cal_toppings_price) {
        this.cal_toppings_price = cal_toppings_price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getMedium_price() {
        return medium_price;
    }

    public void setMedium_price(String medium_price) {
        this.medium_price = medium_price;
    }

    public String getLarger_price() {
        return larger_price;
    }

    public void setLarger_price(String larger_price) {
        this.larger_price = larger_price;
    }

    public String getRegular_price() {
        return regular_price;
    }

    public void setRegular_price(String regular_price) {
        this.regular_price = regular_price;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getPrice() {
        return price;
    }

    public void setprice(int price) {
        this.price = price;
    }

    public boolean isAddToCart() {
        return isAddToCart;
    }

    public void setAddToCart(boolean addToCart) {
        isAddToCart = addToCart;
    }
}
