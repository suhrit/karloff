package com.inventrice.karloff.Model;

/**
 * Created by matainja on 26-Oct-17.
 */

public class ModelOfflineCart {

    String date;
    String time;
    String chzStat;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getChzStat() {
        return chzStat;
    }

    public void setChzStat(String chzStat) {
        this.chzStat = chzStat;
    }

    public String getExtrChzPrice() {
        return extrChzPrice;
    }

    public void setExtrChzPrice(String extrChzPrice) {
        this.extrChzPrice = extrChzPrice;
    }

    String extrChzPrice;
    String id;
    String name;
    String desc;
    String rate;
    String qty;
    String price;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
