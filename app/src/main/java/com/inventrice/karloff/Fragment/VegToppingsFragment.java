package com.inventrice.karloff.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.inventrice.karloff.Model.ModelTempToppingsStatus;
import com.inventrice.karloff.Model.ModelToppings;
import com.inventrice.karloff.R;
import com.inventrice.karloff.Util.Constants;
import com.inventrice.karloff.database.DatabaseAdapter;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by suhrit on 10-09-2017.
 */

public class VegToppingsFragment extends Fragment implements View.OnClickListener{

    View fragmentView = null;
    public static String catID;
    RecyclerView lv;
    static String name = null;
    static String email=null;
    static DatabaseAdapter dbHelper;
    static RelativeLayout main;
    ProgressBar progressBar;
    ArrayList<ModelToppings> productArrayList = new ArrayList<ModelToppings>();
    static ModelTempToppingsStatus mts = new ModelTempToppingsStatus();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        fragmentView = inflater.inflate(R.layout.layout_menu_type_fragment1, null);

        dbHelper = new DatabaseAdapter(getActivity());
        dbHelper.open();

        main = (RelativeLayout) fragmentView.findViewById(R.id.main);
        lv = (RecyclerView) fragmentView.findViewById(R.id.productlist);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        lv .setLayoutManager(mLayoutManager);
        lv.setNestedScrollingEnabled(false);
        lv.setVisibility(View.VISIBLE);
        Log.e("catID ",catID);

        progressBar = (ProgressBar) fragmentView.findViewById(R.id.progressBar);
        FetchProduct();
        return fragmentView;
    }

    public void FetchProduct(){

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.PRODUCT_TOPPINGS_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Toast.makeText(mContext,response,Toast.LENGTH_LONG).show();
                        Log.e("response ** ",response);
                        progressBar.setVisibility(View.GONE);
                        productArrayList = new ArrayList<ModelToppings>();
                        try {
                            JSONObject jsonObj = new JSONObject(response);
                            if(jsonObj.getString("status").equals("1")){
                                for(int i = 0; i < jsonObj.getJSONArray("result").length();i++){
                                    ModelToppings mt = new ModelToppings();
                                    JSONObject js = jsonObj.getJSONArray("result").getJSONObject(i);
                                    mt.setId(js.getString("id"));
                                    mt.setName(js.getString("name"));
                                    mt.setTp_type(js.getString("tp_type"));
                                    mt.setImg_path(js.getString("img_path"));
                                    mt.setRate(js.getString("rate"));

                                    Log.e("rate == ",js.getString("rate"));
                                    productArrayList.add(mt);
                                }
                                ProductAdapter adapter=new ProductAdapter(getActivity(), productArrayList);
                                lv.setAdapter(adapter);

                            }else{
                                progressBar.setVisibility(View.GONE);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressBar.setVisibility(View.GONE);
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressBar.setVisibility(View.GONE);
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("catID","1");
                Log.e("params==",""+params);
                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }

    public static class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ProductAdapterHolder>{
        private Context context;
        private List<ModelToppings> productlist;

        public ProductAdapter(Context context,List<ModelToppings> productlist) {
            // TODO Auto-generated constructor stub

            this.context = context;
            this.productlist = productlist;


            Log.e("ksdf cons","ldhf");
        }
        @Override
        public ProductAdapter.ProductAdapterHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.toppings_list_row, parent, false);
            Log.e("ksdf","ldhf");
            return new ProductAdapter.ProductAdapterHolder(view);
        }

        @Override
        public void onBindViewHolder(final ProductAdapter.ProductAdapterHolder holder, final int position) {


            Log.e("name",productlist.get(position).getName());
            holder.name.setText(productlist.get(position).getName());

            Picasso.with(context).load(Constants.BASE_URL+"/"+productlist.get(position).getImg_path()).into(holder.product_image);

            Log.e("chkbox stat== ","id "+productlist.get(position).getId()+" "+dbHelper.checkToppingsStatus(productlist.get(position).getId(),catID));
            holder.chk.setChecked(dbHelper.checkToppingsStatus(productlist.get(position).getId(), catID));
            productlist.get(position).setChecked(dbHelper.checkToppingsStatus(productlist.get(position).getId(), catID));
            holder.chk.setTag(Integer.valueOf(position));
            holder.chk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    Log.e("price",""+productlist.get(position).isChecked());
                    if(productlist.get(position).isChecked()){
                        productlist.get(position).setChecked(false);
                        dbHelper.deleteToppingsItem(productlist.get(position).getId(), catID);
                        Log.e("del ","TRUE");
                        Snackbar snackbar = Snackbar
                                .make(main, "Toppings Removed", Snackbar.LENGTH_LONG);

                        snackbar.show();
                    }else {
                        productlist.get(position).setChecked(true);
                        mts.setProduct_id(catID);
                        mts.setToppings_price(productlist.get(position).getRate());
                        mts.setToppings_id(productlist.get(position).getId());
                        mts.setToppings_name(productlist.get(position).getName());
                        mts.setCart_status("Not Added");
                        boolean status = dbHelper.InsertToppingsInfo(mts);
                        Log.e("status", "" + status);
                        Snackbar snackbar = Snackbar
                                .make(main, "Toppings Added", Snackbar.LENGTH_LONG);

                        snackbar.show();
                    }
                }
            });

            /*holder.chk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });*/



        }

        @Override
        public int getItemCount() {
            return productlist.size();
        }

        class ProductAdapterHolder extends RecyclerView.ViewHolder {
            TextView name;
            ImageView product_image;
            CheckBox chk;

            public ProductAdapterHolder(View v) {
                super(v);

                name = (TextView) v.findViewById(R.id.name);
                product_image = (ImageView) v.findViewById(R.id.product_image);
                chk = (CheckBox) v.findViewById(R.id.chk);

            }
        }

        public interface OnItemClickListener {
            public void onItemClick(View view, int position);
        }
        public int calculatePrice(int count,int price){
            int total;
            total = count*price;
            Log.e("TOTAL = ",""+total);
            //holder.price.setText("Rs "+productlist.get(position).getPrice());
            return total;
        }
    }
    @Override
    public void onClick(View view) {

    }
}
