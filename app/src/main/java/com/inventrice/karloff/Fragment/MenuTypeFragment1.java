package com.inventrice.karloff.Fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.inventrice.karloff.ActivityMenuDetails;
import com.inventrice.karloff.ActivityToppings;
import com.inventrice.karloff.Model.ModelCategory;
import com.inventrice.karloff.Model.ModelItemDetails;
import com.inventrice.karloff.Model.ModelOfflineCart;
import com.inventrice.karloff.Model.ModelProduct;
import com.inventrice.karloff.Model.ModelSubCategory;
import com.inventrice.karloff.Model.ModelToppings;
import com.inventrice.karloff.R;
import com.inventrice.karloff.Util.Constants;
import com.inventrice.karloff.WelcomeActivity;
import com.inventrice.karloff.database.DatabaseAdapter;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by matainja on 02-Sep-17.
 */

public class MenuTypeFragment1 extends Fragment implements View.OnClickListener{

    View fragmentView = null;
    public static String catID;
    RecyclerView  lv;
    static String name = null;
    static String email=null;
    static String phn=null;
    static DatabaseAdapter dbHelper;
    ProgressDialog pd;
    ArrayList<ModelProduct> productArrayList = new ArrayList<ModelProduct>();
    ProductAdapter adapter;
    RelativeLayout main;
    static ModelItemDetails mid = new ModelItemDetails();
    SwipeRefreshLayout swipeRefreshLayout;
    String product_name,product_desc,rate,qty,full_amount,order_id;
    ProgressBar progressBar;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        fragmentView = inflater.inflate(R.layout.layout_menu_type_fragment1, null);
        dbHelper = new DatabaseAdapter(getActivity());
        dbHelper.open();


        SharedPreferences prefs = getActivity().getSharedPreferences("login_pref", getActivity().MODE_PRIVATE);


        name = prefs.getString("name", "NULL");//"No name defined" is the default value.
        email = prefs.getString("email", "NULL"); //0 is the default value.
        phn = prefs.getString("phn", "NULL");

        main = (RelativeLayout) fragmentView.findViewById(R.id.main);
        lv = (RecyclerView) fragmentView.findViewById(R.id.productlist);



        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        lv .setLayoutManager(mLayoutManager);
        lv.setNestedScrollingEnabled(false);
        lv.setVisibility(View.VISIBLE);

        adapter=new ProductAdapter(getActivity(), productArrayList);
        lv.setAdapter(adapter);
        //ProductAdapter adapter=new ProductAdapter(getActivity(), productArrayList);
        //lv.setAdapter(adapter);
        progressBar = (ProgressBar) fragmentView.findViewById(R.id.progressBar);
        FetchProduct();
        return fragmentView;



    }

    @Override
    public void onResume(){
        super.onResume();
        //OnResume Fragment
        lv.setAdapter(adapter);
        //adapter.notifyDataSetChanged();
        //Log.e("Fragment ","ACTIVE");
    }
    public void FetchProduct(){

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.PRODUCT_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Toast.makeText(mContext,response,Toast.LENGTH_LONG).show();
                        Log.e("response ** ",response);
                        productArrayList = new ArrayList<ModelProduct>();
                        try {
                            JSONObject jsonObj = new JSONObject(response);
                            if(jsonObj.getString("status").equals("1")){
                                progressBar.setVisibility(View.GONE);
                                for(int i = 0; i < jsonObj.getJSONArray("result").length();i++){
                                    ModelProduct mp = new ModelProduct();
                                    JSONObject js = jsonObj.getJSONArray("result").getJSONObject(i);
                                    mp.setId(js.getString("id"));
                                    mp.setName(js.getString("product_name"));
                                    mp.setDesc(js.getString("product_desc"));
                                    mp.setImage(js.getString("product_img_path"));
                                    mp.setLarger_price(js.getString("product_mrp_r"));
                                    mp.setMedium_price(js.getString("product_mrp_m"));
                                    mp.setRegular_price(js.getString("product_mrp_l"));
                                    mp.setExtrChzPrice(js.getString("extrachz"));
                                    mp.setCount(1);

                                    productArrayList.add(mp);
                                }

                                adapter=new ProductAdapter(getActivity(), productArrayList);
                                lv.setAdapter(adapter);

                                //CLA.notifyDataSetChanged();

                            }else{
                                progressBar.setVisibility(View.GONE);
                                //Toast.makeText(WelcomeActivity.this,"Account Not Found...",Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressBar.setVisibility(View.GONE);
                        }
                        //ProductAdapter adapter=new ProductAdapter(getActivity(), productArrayList);
                        //lv.setAdapter(adapter);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Toast.makeText(mContext,error.toString(),Toast.LENGTH_LONG).show();
                        progressBar.setVisibility(View.GONE);
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("catID",catID);
                params.put("cust_id",phn);
                Log.e("params==",""+params);
                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }


    public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ProductAdapterHolder>{
        private Context context;
        private List<ModelProduct> productlist;

        public ProductAdapter(Context context,List<ModelProduct> productlist) {
            // TODO Auto-generated constructor stub

            this.context = context;
            this.productlist = productlist;


            //Log.e("ksdf cons","ldhf");
        }
        @Override
        public ProductAdapter.ProductAdapterHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_list_row, parent, false);
            //Log.e("ksdf","ldhf");
            return new ProductAdapterHolder(view);
        }

        @Override
        public void onBindViewHolder(final ProductAdapter.ProductAdapterHolder holder, final int position) {

            double toppings_price = 0.00;
            if(dbHelper.checkIfToppingsAdded(productlist.get(position).getId())){
                holder.toppings_txt.setText("Toppings Added");
                toppings_price = dbHelper.CalculateToppingsPrice(productlist.get(position).getId());
                productlist.get(position).setCal_toppings_price(toppings_price);
                Log.e("total price",""+toppings_price);
            }

            holder.productName.setText(productlist.get(position).getName());
            holder.prod_desc.setText(productlist.get(position).getDesc());
            holder.price.setText("Rs " + productlist.get(position).getMedium_price());
            holder.med_price.setText(productlist.get(position).getMedium_price());
            holder.lar_price.setText(productlist.get(position).getLarger_price());
            holder.reg_price.setText(productlist.get(position).getRegular_price());

            holder.med_price.setTextColor(context.getResources().getColor(R.color.siteGreenColor));
            productlist.get(position).setprice(Integer.valueOf(productlist.get(position).getMedium_price()));
            Log.e("img == ",Constants.BASE_URL + "/" + productlist.get(position).getImage());

            String prod_img = Constants.BASE_URL + "/" + productlist.get(position).getImage();
            prod_img = prod_img.replaceAll(" ", "%20");

            Picasso.with(context).load(prod_img).into(holder.product_image);

            mid.setItem_type("medium");
            mid.setExtra_cheese(false);
            mid.setItem_total("1");
            mid.setItem_total_price(productlist.get(position).getMedium_price());

            if(dbHelper.checkItemDetailsInfo(productlist.get(position).getId()).size()>0){
                ArrayList<ModelItemDetails> list = new ArrayList<ModelItemDetails>();
                list = dbHelper.checkItemDetailsInfo(productlist.get(position).getId());

                Log.e("list size ",""+list.size());
                Log.e("position == ",list.get(0).getItem_total());

                //try {
                    //holder.price.setText("Rs " + list.get(position).getItem_total_price());
                    holder.txt_counter.setText(list.get(0).getItem_total());
                    productlist.get(position).setCount(Integer.parseInt(list.get(0).getItem_total()));
                    holder.extraChz_chk.setChecked(list.get(0).isExtra_cheese());

                int temp_price;
                    if (list.get(0).getItem_type().equalsIgnoreCase("medium")) {

                        holder.med_chk.setChecked(true);
                        holder.med_price.setTextColor(context.getResources().getColor(R.color.siteGreenColor));
                        //productlist.get(position).setprice(Integer.parseInt(list.get(position).getItem_total_price()));
                        holder.price.setText("Rs "+calculatePrice(Integer.parseInt(list.get(0).getItem_total()),Integer.parseInt(productlist.get(position).getMedium_price()),productlist.get(position).getCal_toppings_price()));

                    } else if (list.get(0).getItem_type().equalsIgnoreCase("large")) {
                        holder.lar_chk.setChecked(true);
                        holder.lar_price.setTextColor(context.getResources().getColor(R.color.siteGreenColor));
                        //productlist.get(position).setprice(Integer.parseInt(list.get(position).getItem_total_price()));
                        holder.price.setText("Rs "+calculatePrice(Integer.parseInt(list.get(0).getItem_total()),Integer.parseInt(productlist.get(position).getLarger_price()),productlist.get(position).getCal_toppings_price()));

                    } else if (list.get(0).getItem_type().equalsIgnoreCase("regular")) {
                        holder.reg_chk.setChecked(true);
                        holder.reg_price.setTextColor(context.getResources().getColor(R.color.siteGreenColor));
                        //productlist.get(position).setprice(Integer.parseInt(list.get(position).getItem_total_price()));
                        holder.price.setText("Rs "+calculatePrice(Integer.parseInt(list.get(0).getItem_total()),Integer.parseInt(productlist.get(position).getRegular_price()),productlist.get(position).getCal_toppings_price()));

                    } else {
                        holder.med_chk.setChecked(true);
                        holder.med_price.setTextColor(context.getResources().getColor(R.color.siteGreenColor));
                        //productlist.get(position).setprice(Integer.parseInt(list.get(position).getItem_total_price()));
                        holder.price.setText("Rs "+calculatePrice(Integer.parseInt(list.get(0).getItem_total()),Integer.parseInt(productlist.get(position).getMedium_price()),productlist.get(position).getCal_toppings_price()));

                    }
                /*}catch(Exception e){
                    holder.price.setText("Rs " + productlist.get(position).getMedium_price());
                    holder.txt_counter.setText("0");
                    holder.extraChz_chk.setChecked(false);

                    holder.med_chk.setChecked(true);
                    holder.med_price.setTextColor(context.getResources().getColor(R.color.siteGreenColor));
                    productlist.get(position).setprice(Integer.valueOf(productlist.get(position).getMedium_price()));
                }*/

            }else {
                  holder.med_chk.setChecked(true);

                /*holder.price.setText("Rs " + productlist.get(position).getMedium_price());

                Picasso.with(context).load(Constants.BASE_URL + "/" + productlist.get(position).getImage()).into(holder.product_image);
*/


            }


            holder.med_chk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    productlist.get(position).setprice(Integer.valueOf(productlist.get(position).getMedium_price()));
                    holder.price.setText("Rs "+calculatePrice(productlist.get(position).getCount(),productlist.get(position).getPrice(),productlist.get(position).getCal_toppings_price()));
                    holder.lar_chk.setChecked(false);
                    holder.reg_chk.setChecked(false);

                    mid.setProduct_id(productlist.get(position).getId());
                    mid.setItem_type("medium");
                }
            });
            holder.lar_chk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    productlist.get(position).setprice(Integer.valueOf(productlist.get(position).getLarger_price()));
                    holder.price.setText("Rs "+calculatePrice(productlist.get(position).getCount(),productlist.get(position).getPrice(),productlist.get(position).getCal_toppings_price()));
                    holder.med_chk.setChecked(false);
                    holder.reg_chk.setChecked(false);

                    mid.setProduct_id(productlist.get(position).getId());
                    mid.setItem_type("large");
                }
            });
            holder.reg_chk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    productlist.get(position).setprice(Integer.valueOf(productlist.get(position).getRegular_price()));
                    holder.price.setText("Rs "+calculatePrice(productlist.get(position).getCount(),productlist.get(position).getPrice(),productlist.get(position).getCal_toppings_price()));
                    holder.lar_chk.setChecked(false);
                    holder.med_chk.setChecked(false);

                    mid.setProduct_id(productlist.get(position).getId());
                    mid.setItem_type("regular");
                }
            });

            holder.extraChz_chk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    //Log.e("is Extra Cheese",""+isChecked);
                    if(isChecked) {
                        mid.setProduct_id(productlist.get(position).getId());
                        mid.setExtra_cheese(isChecked);
                        Snackbar snackbar = Snackbar
                                .make(main, "Extra Cheese Added", Snackbar.LENGTH_LONG);

                        snackbar.show();

                    }else{
                        mid.setProduct_id(productlist.get(position).getId());
                        mid.setExtra_cheese(isChecked);
                        Snackbar snackbar = Snackbar
                                .make(main, "Extra Cheese Removed", Snackbar.LENGTH_LONG);

                        snackbar.show();
                    }
                }
            });
            holder.minus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(productlist.get(position).getCount() > 1){
                        //int count;
                        productlist.get(position).setCount(productlist.get(position).getCount()-1);
                        holder.txt_counter.setText(String.valueOf(productlist.get(position).getCount()));
                        holder.price.setText("Rs "+calculatePrice(productlist.get(position).getCount(),productlist.get(position).getPrice(),productlist.get(position).getCal_toppings_price()));
                        mid.setItem_total(holder.txt_counter.getText().toString());
                        mid.setProduct_id(productlist.get(position).getId());
                    }
                }
            });

           // Log.e("plus position out ",""+position+" "+productlist.get(position).getPrice());
            holder.plus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //if(productlist.get(position).getCount() != 0){
                        //int count;
                   // Log.e("plus position ",""+position+" "+productlist.get(position).getPrice());
                    productlist.get(position).setCount(productlist.get(position).getCount()+1);
                    holder.txt_counter.setText(String.valueOf(productlist.get(position).getCount()));
                    holder.price.setText("Rs "+calculatePrice(productlist.get(position).getCount(),productlist.get(position).getPrice(),productlist.get(position).getCal_toppings_price()));
                    //}
                    mid.setProduct_id(productlist.get(position).getId());
                    mid.setItem_total(holder.txt_counter.getText().toString());
                }
            });

            holder.add_to_cart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
                    Long tsLong = System.currentTimeMillis()/1000;
                    String ts = tsLong.toString();
                    Log.e("date ",date+" "+ts);

                    product_name = productlist.get(position).getName();
                    product_desc = productlist.get(position).getDesc();
                    final String prod_id = productlist.get(position).getId();
                    //order_id = productlist.get(position).getOrder_id();
                    if(holder.med_chk.isChecked()) {
                        rate = productlist.get(position).getMedium_price();
                        qty = holder.txt_counter.getText().toString();
                        full_amount = String.valueOf(Double.valueOf(qty)*Double.valueOf(rate));
                        Log.e("IF ","TRUE");
                    }else if(holder.lar_chk.isChecked()) {
                        rate = productlist.get(position).getLarger_price();
                        qty = holder.txt_counter.getText().toString();
                        full_amount = String.valueOf(Double.valueOf(qty)*Double.valueOf(rate));
                        Log.e("ELSE IF ","TRUE");
                    } else if(holder.reg_chk.isChecked()) {
                        rate = productlist.get(position).getRegular_price();
                        qty = holder.txt_counter.getText().toString();
                        full_amount = String.valueOf(Double.valueOf(qty)*Double.valueOf(rate));
                        Log.e("ELSE ","TRUE");
                    }

                    if(!email.equalsIgnoreCase("NULL")) {
                        Log.e("size ",""+dbHelper.getAddedToppingsByProduct(productlist.get(position).getId()).size());


                        Log.e("RATE ",rate);
                        Log.e("qty ",qty);
                        Log.e("full_amount ",full_amount);
                        Log.e("product_name ",product_name);
                        Log.e("product_desc ",product_desc);
                        //Log.e("order_id ",order_id);


                        if(holder.extraChz_chk.isChecked()) {
                            Constants.CART_COUNT = Constants.CART_COUNT+1+1;
                            Log.e("cart count",""+Constants.CART_COUNT);
                            AddToCart(prod_id, product_name, product_desc, rate, qty, full_amount, date, ts,true, productlist.get(position).getExtrChzPrice());
                        }else{
                            Constants.CART_COUNT = Constants.CART_COUNT+1;
                            Log.e("cart count",""+Constants.CART_COUNT);
                            AddToCart(prod_id, product_name, product_desc, rate, qty, full_amount, date, ts,false, "0");
                        }

                        if (dbHelper.getAddedToppingsByProduct(productlist.get(position).getId()).size()==0) {
                            Log.e("IF","TRUE");
                            //holder.add_to_cart_txt.setText("Item Added");
                            //productlist.get(position).setAddToCart(false);



                        } else {
                            Log.e("ELSE","TRUE "+holder.med_chk.isChecked());
                            //holder.add_to_cart_txt.setText("Item Added");
                            //productlist.get(position).setAddToCart(true);


                        }
                    }else{
                        /*Intent i = new Intent(context, WelcomeActivity.class);
                        context.startActivity(i);
                        ((Activity)context).finish();*/
                        ModelOfflineCart moc = new ModelOfflineCart();
                        moc.setId(prod_id);
                        moc.setName(product_name);
                        moc.setDesc(product_desc);
                        moc.setDate(date);
                        moc.setQty(qty);
                        moc.setPrice(full_amount);
                        moc.setRate(rate);
                        moc.setTime(ts);
                        if(holder.extraChz_chk.isChecked()) {
                            moc.setExtrChzPrice(productlist.get(position).getExtrChzPrice());
                            moc.setChzStat("true");
                        }else{
                            moc.setExtrChzPrice("0");
                            moc.setChzStat("false");
                        }


                        boolean insertcart = dbHelper.InsertItemToCart(moc);
                        if(holder.extraChz_chk.isChecked()) {
                            Constants.CART_COUNT = Constants.CART_COUNT + 1 + 1;
                        }else{
                            Constants.CART_COUNT = Constants.CART_COUNT + 1;
                        }
                        Log.e("insertcart ",""+insertcart);
                        if(dbHelper.getAddedToppingsByProduct(productlist.get(position).getId()).size()==0){

                            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    switch (which){
                                        case DialogInterface.BUTTON_POSITIVE:
                                            //Yes button clicked


                                            Intent i = new Intent(getActivity(),ActivityToppings.class);
                                            i.putExtra("callfrom","toppingsbtn");
                                            VegToppingsFragment.catID = productlist.get(position).getId();
                                            NonVegToppingsFragment.catID = productlist.get(position).getId();
                                            context.startActivity(i);
                                            //((Activity)context).finish();
                                            break;

                                        case DialogInterface.BUTTON_NEGATIVE:
                                            //No button clicked

                                            Toast.makeText(getActivity(),"Item Added To Cart",Toast.LENGTH_LONG).show();
                                            /*Snackbar snackbar = Snackbar
                                                    .make(main, "Item Added To Cart", Snackbar.LENGTH_LONG);

                                            snackbar.show();*/

                                            ArrayList<ModelSubCategory> mscList = new ArrayList<ModelSubCategory>();
                                            ModelSubCategory msc1 = new ModelSubCategory();
                                            ModelSubCategory msc2 = new ModelSubCategory();
                                            ModelCategory mc = new ModelCategory();
                                            msc1.setSubCatID("2");
                                            msc2.setSubCatID("3");
                                            mscList.add(msc1);
                                            mscList.add(msc2);
                                            mc.setSubCatArray(mscList);


                                            Intent i1 = new Intent(getActivity(), ActivityMenuDetails.class);
                                            i1.putExtra("isSubCat","true");
                                            i1.putExtra("SubCat",(ArrayList<ModelSubCategory>)mc.getSubCatArray());
                                            startActivity(i1);
                                            getActivity().finish();
                                            break;
                                    }
                                }
                            };

                            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            builder.setMessage("Do you want to add topppings?").setPositiveButton("Yes", dialogClickListener)
                                    .setNegativeButton("No", dialogClickListener).show();

                        }else{

                        }
                    }
                }
            });

            holder.buyNow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
                    Long tsLong = System.currentTimeMillis()/1000;
                    String ts = tsLong.toString();
                    Log.e("date ",date+" "+ts);

                    product_name = productlist.get(position).getName();
                    product_desc = productlist.get(position).getDesc();
                    final String prod_id = productlist.get(position).getId();
                    //order_id = productlist.get(position).getOrder_id();
                    if(holder.med_chk.isChecked()) {
                        rate = productlist.get(position).getMedium_price();
                        qty = holder.txt_counter.getText().toString();
                        full_amount = String.valueOf(Double.valueOf(qty)*Double.valueOf(rate));
                        Log.e("IF ","TRUE");
                    }else if(holder.lar_chk.isChecked()) {
                        rate = productlist.get(position).getLarger_price();
                        qty = holder.txt_counter.getText().toString();
                        full_amount = String.valueOf(Double.valueOf(qty)*Double.valueOf(rate));
                        Log.e("ELSE IF ","TRUE");
                    } else if(holder.reg_chk.isChecked()) {
                        rate = productlist.get(position).getRegular_price();
                        qty = holder.txt_counter.getText().toString();
                        full_amount = String.valueOf(Double.valueOf(qty)*Double.valueOf(rate));
                        Log.e("ELSE ","TRUE");
                    }

                    if(!email.equalsIgnoreCase("NULL")) {
                        Log.e("size ",""+dbHelper.getAddedToppingsByProduct(productlist.get(position).getId()).size());


                        Log.e("RATE ",rate);
                        Log.e("qty ",qty);
                        Log.e("full_amount ",full_amount);
                        Log.e("product_name ",product_name);
                        Log.e("product_desc ",product_desc);
                        //Log.e("order_id ",order_id);


                        if(holder.extraChz_chk.isChecked()) {
                            Constants.CART_COUNT = Constants.CART_COUNT+1+1;
                            Log.e("cart count",""+Constants.CART_COUNT);
                            AddToCart(prod_id, product_name, product_desc, rate, qty, full_amount, date, ts,true, productlist.get(position).getExtrChzPrice());
                        }else{
                            Constants.CART_COUNT = Constants.CART_COUNT+1;
                            Log.e("cart count",""+Constants.CART_COUNT);
                            AddToCart(prod_id, product_name, product_desc, rate, qty, full_amount, date, ts,false, "0");
                        }

                        if (dbHelper.getAddedToppingsByProduct(productlist.get(position).getId()).size()==0) {
                            Log.e("IF","TRUE");
                            //holder.add_to_cart_txt.setText("Item Added");
                            //productlist.get(position).setAddToCart(false);



                        } else {
                            Log.e("ELSE","TRUE "+holder.med_chk.isChecked());
                            //holder.add_to_cart_txt.setText("Item Added");
                            //productlist.get(position).setAddToCart(true);


                        }
                    }else{
                        Intent i = new Intent(context, WelcomeActivity.class);
                        context.startActivity(i);
                        ((Activity)context).finish();


                        /*ModelOfflineCart moc = new ModelOfflineCart();
                        moc.setId(prod_id);
                        moc.setName(product_name);
                        moc.setDesc(product_desc);
                        moc.setDate(date);
                        moc.setQty(qty);
                        moc.setPrice(full_amount);
                        moc.setRate(rate);
                        moc.setTime(ts);
                        if(holder.extraChz_chk.isChecked()) {
                            moc.setExtrChzPrice(productlist.get(position).getExtrChzPrice());
                            moc.setChzStat("true");
                        }else{
                            moc.setExtrChzPrice("0");
                            moc.setChzStat("false");
                        }


                        boolean insertcart = dbHelper.InsertItemToCart(moc);
                        if(holder.extraChz_chk.isChecked()) {
                            Constants.CART_COUNT = Constants.CART_COUNT + 1 + 1;
                        }else{
                            Constants.CART_COUNT = Constants.CART_COUNT + 1;
                        }
                        Log.e("insertcart ",""+insertcart);
                        if(dbHelper.getAddedToppingsByProduct(productlist.get(position).getId()).size()==0){

                            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    switch (which){
                                        case DialogInterface.BUTTON_POSITIVE:
                                            //Yes button clicked


                                            Intent i = new Intent(getActivity(),ActivityToppings.class);
                                            i.putExtra("callfrom","toppingsbtn");
                                            VegToppingsFragment.catID = productlist.get(position).getId();
                                            NonVegToppingsFragment.catID = productlist.get(position).getId();
                                            context.startActivity(i);
                                            //((Activity)context).finish();
                                            break;

                                        case DialogInterface.BUTTON_NEGATIVE:
                                            //No button clicked

                                            Toast.makeText(getActivity(),"Item Added To Cart",Toast.LENGTH_LONG).show();
                                            *//*Snackbar snackbar = Snackbar
                                                    .make(main, "Item Added To Cart", Snackbar.LENGTH_LONG);

                                            snackbar.show();*//*

                                            ArrayList<ModelSubCategory> mscList = new ArrayList<ModelSubCategory>();
                                            ModelSubCategory msc1 = new ModelSubCategory();
                                            ModelSubCategory msc2 = new ModelSubCategory();
                                            ModelCategory mc = new ModelCategory();
                                            msc1.setSubCatID("2");
                                            msc2.setSubCatID("3");
                                            mscList.add(msc1);
                                            mscList.add(msc2);
                                            mc.setSubCatArray(mscList);


                                            Intent i1 = new Intent(getActivity(), ActivityMenuDetails.class);
                                            i1.putExtra("isSubCat","true");
                                            i1.putExtra("SubCat",(ArrayList<ModelSubCategory>)mc.getSubCatArray());
                                            startActivity(i1);
                                            getActivity().finish();
                                            break;
                                    }
                                }
                            };

                            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            builder.setMessage("Do you want to add topppings?").setPositiveButton("Yes", dialogClickListener)
                                    .setNegativeButton("No", dialogClickListener).show();

                        }else{

                        }*/
                    }

                }
            });

            holder.toppings.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(context, ActivityToppings.class);
                    i.putExtra("callfrom","toppingsbtn");
                    VegToppingsFragment.catID = productlist.get(position).getId();
                    NonVegToppingsFragment.catID = productlist.get(position).getId();
                    context.startActivity(i);
                    boolean createSuccessful = dbHelper.InsertItemDetailsInfo(mid);
                   // Log.e("createSuccessful == ",""+createSuccessful);
                    //((Activity)context).finish();
                }
            });
        }

        @Override
        public int getItemCount() {
            return productlist.size();
        }

        class ProductAdapterHolder extends RecyclerView.ViewHolder {
            TextView productName,prod_desc,price,med_price,lar_price,reg_price,add_to_cart_txt,toppings_txt,txt_counter;
            ImageView product_image,minus,plus;
            CheckBox med_chk,lar_chk,reg_chk,extraChz_chk;
            LinearLayout add_to_cart,toppings,buyNow;
            public ProductAdapterHolder(View v) {
                super(v);

                productName = (TextView) v.findViewById(R.id.productName);
                prod_desc = (TextView) v.findViewById(R.id.prod_desc);
                price = (TextView) v.findViewById(R.id.price);
                med_price = (TextView) v.findViewById(R.id.med_price);
                lar_price = (TextView) v.findViewById(R.id.lar_price);
                reg_price = (TextView) v.findViewById(R.id.reg_price);
                txt_counter = (TextView) v.findViewById(R.id.txt_counter);
                add_to_cart_txt = (TextView) v.findViewById(R.id.add_to_cart_txt);
                toppings_txt = (TextView) v.findViewById(R.id.toppings_txt);

                product_image = (ImageView) v.findViewById(R.id.product_image);
                minus = (ImageView) v.findViewById(R.id.minus);
                plus = (ImageView) v.findViewById(R.id.plus);

                med_chk = (CheckBox) v.findViewById(R.id.med_chk);
                lar_chk = (CheckBox) v.findViewById(R.id.lar_chk);
                reg_chk = (CheckBox) v.findViewById(R.id.reg_chk);
                extraChz_chk = (CheckBox) v.findViewById(R.id.extraChz_chk);


                add_to_cart = (LinearLayout) v.findViewById(R.id.add_to_cart);
                toppings = (LinearLayout) v.findViewById(R.id.toppings);
                buyNow = (LinearLayout) v.findViewById(R.id.buyNow);
            }
        }

        /*public interface OnItemClickListener {
            public void onItemClick(View view, int position);
        }*/
        public double calculatePrice(int count,int price, double toppings_price){
            double total;
            Log.e("cal == ",""+count+" * "+price+" + "+toppings_price);
            total = (count*price)+toppings_price;
            Log.e("TOTAL = ",""+total);
            //holder.price.setText("Rs "+productlist.get(position).getPrice());
            return total;
        }
    }
    @Override
    public void onClick(View v) {

    }

    public void AddToCart(final String product_id, final String product_name, final String product_desc, final String rate, final String qty, final String full_amount, final String date, final String timestamp, final boolean extrachzStat, final String extrachzPrice){
        pd = new ProgressDialog(getActivity(),ProgressDialog.THEME_HOLO_DARK);
        pd.setMessage("Please Wait...");
        pd.setCancelable(false);
        pd.show();

        Log.e("cart ","function");
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.ADDTOCART,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Toast.makeText(mContext,response,Toast.LENGTH_LONG).show();
                        Log.e("response ** ",response);
                        try {
                            JSONObject jsonObj = new JSONObject(response);
                            if(jsonObj.getString("status").equals("1")){
                                /*Log.e("NAME",jsonObj.getString("name")+" "+jsonObj.getString("email"));
                                *//*ModelSession ses = new ModelSession(WelcomeActivity.this);
                                ses.setName(jsonObj.getString("name"),jsonObj.getString("email"));*/
                                String order_id = jsonObj.getString("order_id");
                                String toppings_desc = jsonObj.getString("product_name");

                                AddToppingsToCart(product_id,order_id,toppings_desc);


                                //pd.hide();

                            }else{

                                pd.hide();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            pd.hide();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Toast.makeText(mContext,error.toString(),Toast.LENGTH_LONG).show();
                        pd.hide();
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("cust_id", phn);
                params.put("prod_type", catID);
                params.put("product_name", product_name);
                params.put("product_desc", product_desc);
                params.put("rate", rate);
                params.put("qty", qty);
                params.put("full_amount", full_amount);
                params.put("date", date);
                params.put("timestamp", timestamp);
                if(extrachzStat){
                    params.put("extrachz", String.valueOf(extrachzStat));
                    params.put("extrachzPrice", extrachzPrice);
                }else{
                    params.put("extrachz", String.valueOf(extrachzStat));
                }
                Log.e("params==",""+params);
                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,-1,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    public void AddToppingsToCart(final String product_id, final String order_id, final String toppings_desc){
        ArrayList<ModelToppings> list = new ArrayList<ModelToppings>();
        list = dbHelper.getAddedToppingsByProduct(product_id);
        Log.e("toppings size ",""+list.size());
        Constants.CART_COUNT = Constants.CART_COUNT+list.size();
        Log.e("cart count",""+Constants.CART_COUNT);
        final ArrayList<ModelToppings> temp_list = list;
        if(list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {

                final int count = i + 1;


                final String topp_name = list.get(i).getName();
                final String topp_rate = list.get(i).getRate();
//Constants.ADDTOCART_TOPPINGS
                StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.ADDTOCART_TOPPINGS,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                //Toast.makeText(mContext,response,Toast.LENGTH_LONG).show();
                                Log.e("response ** ", response);
                                try {
                                    JSONObject jsonObj = new JSONObject(response);
                                    if (jsonObj.getString("status").equals("1")) {
                                        Log.e("count ", "" + count);
                                        if (count == temp_list.size()) {
                                            Snackbar snackbar = Snackbar
                                                    .make(main, "Item Added To Cart", Snackbar.LENGTH_LONG);

                                            snackbar.show();

                                            dbHelper.deleteToppingsItemByProduct(product_id);
                                            pd.hide();

                                            ArrayList<ModelSubCategory> mscList = new ArrayList<ModelSubCategory>();
                                            ModelSubCategory msc1 = new ModelSubCategory();
                                            ModelSubCategory msc2 = new ModelSubCategory();
                                            ModelCategory mc = new ModelCategory();
                                            msc1.setSubCatID("2");
                                            msc2.setSubCatID("3");
                                            mscList.add(msc1);
                                            mscList.add(msc2);
                                            mc.setSubCatArray(mscList);


                                            Intent i1 = new Intent(getActivity(), ActivityMenuDetails.class);
                                            i1.putExtra("isSubCat","true");
                                            i1.putExtra("SubCat",(ArrayList<ModelSubCategory>)mc.getSubCatArray());
                                            startActivity(i1);
                                            getActivity().finish();
                                        } else {
                                            Log.e("count ", "remain");
                                        }
                                    } else {

                                        pd.hide();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    pd.hide();
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                //Toast.makeText(mContext,error.toString(),Toast.LENGTH_LONG).show();
                                pd.hide();
                            }
                        }) {
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("cust_id", phn);
                        params.put("order_id", order_id);
                        params.put("product_name", topp_name);
                        params.put("product_desc", toppings_desc);
                        params.put("rate", topp_rate);
                        params.put("full_amount", topp_rate);

                        Log.e("params==", "" + params);
                        return params;
                    }

                };

                RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
                stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,-1,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                requestQueue.add(stringRequest);
            }
        }else{

            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which){
                        case DialogInterface.BUTTON_POSITIVE:
                            //Yes button clicked


                            Intent i = new Intent(getActivity(),ActivityToppings.class);
                            i.putExtra("callfrom","cartbtn");
                            i.putExtra("product_id",product_id);
                            i.putExtra("order_id",order_id);
                            i.putExtra("toppings_desc",toppings_desc);

                            VegToppingsFragment.catID = product_id;
                            NonVegToppingsFragment.catID = product_id;
                            getActivity().startActivity(i);
                            //((Activity)context).finish();
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            //No button clicked

                             Snackbar snackbar = Snackbar
                                                .make(main, "Item Added To Cart", Snackbar.LENGTH_LONG);

                                        snackbar.show();

                            ArrayList<ModelSubCategory> mscList = new ArrayList<ModelSubCategory>();
                            ModelSubCategory msc1 = new ModelSubCategory();
                            ModelSubCategory msc2 = new ModelSubCategory();
                            ModelCategory mc = new ModelCategory();
                            msc1.setSubCatID("2");
                            msc2.setSubCatID("3");
                            mscList.add(msc1);
                            mscList.add(msc2);
                            mc.setSubCatArray(mscList);


                            Intent i1 = new Intent(getActivity(), ActivityMenuDetails.class);
                            i1.putExtra("isSubCat","true");
                            i1.putExtra("SubCat",(ArrayList<ModelSubCategory>)mc.getSubCatArray());
                            startActivity(i1);
                            getActivity().finish();
                            break;
                    }
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage("Do you want to add topppings?").setPositiveButton("Yes", dialogClickListener)
                    .setNegativeButton("No", dialogClickListener).show();

            pd.hide();
        }
    }
}
