package com.inventrice.karloff.Util;

import com.inventrice.karloff.Model.ModelCartItems;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by matainja on 29-Aug-17.
 */

public class Constants {
    public static String BASE_URL = "http://www.inventrice.com/karloff/";
    public static String LOGIN_URL = BASE_URL+"login.php";
    public static String REGISTER_URL = BASE_URL+"register.php";
    public static String CATEGORY_URL = BASE_URL+"category.php";
    public static String PRODUCT_URL = BASE_URL+"product.php";
    public static String PRODUCT_TOPPINGS_URL = BASE_URL+"product_toppings.php";
    public static String PROFILE_SETTINGS = BASE_URL+"profile.php";
    public static String ADDTOCART = BASE_URL+"cart.php";
    public static String ADDTOCART_TOPPINGS = BASE_URL+"toppings_cart.php";
    public static String GET_CART_COUNT = BASE_URL+"cart_count.php";
    public static String GET_CART_ITEMS = BASE_URL+"cart_items.php";
    public static String UPDATE_ADDRESS = BASE_URL+"update_address.php";
    public static String DELETE_CART_ITEMS = BASE_URL+"delete_cart_items.php";
    public static String CHECKOUT = BASE_URL+"checkout.php";
    public static String MY_ORDERS = BASE_URL+"my_orders.php";
    public static String SEND_OTP = BASE_URL+"otp.php";
    public static String LOGIN_PREF = "login_pref";

    public static JSONObject Selected_topping_list = new JSONObject();
    public static JSONArray Selected_topping_Array = new JSONArray();
    public static ArrayList<ModelCartItems> offlineCartItems = new ArrayList<ModelCartItems>();
    public static int CART_COUNT = 0;

}
