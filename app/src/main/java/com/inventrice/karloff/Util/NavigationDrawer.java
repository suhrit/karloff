package com.inventrice.karloff.Util;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.inventrice.karloff.ActivityAboutUs;
import com.inventrice.karloff.ActivityCart;
import com.inventrice.karloff.ActivityDelivery;
import com.inventrice.karloff.ActivityMenu;
import com.inventrice.karloff.ActivityOffers;
import com.inventrice.karloff.ActivityProfile;
import com.inventrice.karloff.ActivityServices;
import com.inventrice.karloff.MainActivity;
import com.inventrice.karloff.R;
import com.inventrice.karloff.WelcomeActivity;

/**
 * Created by matainja on 01-Sep-17.
 */

public class NavigationDrawer extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    public DrawerLayout drawer;
    public ActionBarDrawerToggle toggle;
    public NavigationView navigationView;
    public Toolbar toolbar;
    SharedPreferences prefs;
    TextView nametxt;
    private View headerView;
    public RelativeLayout header_main;
    public String name = null,email=null,phn=null, pin = null, landmark = null, h_no = null,st_name = null;
    public TextView cartCount;
    public LinearLayout cart_count_lay;
    ImageView add_to_cart;
    public void init(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //ModelSeAcssion ses = new ModelSession(MainActivity.this);
        prefs = getSharedPreferences("login_pref", MODE_PRIVATE);


        name = prefs.getString("name", "NULL");//"No name defined" is the default value.
        email = prefs.getString("email", "NULL"); //0 is the default value.
        phn = prefs.getString("phn", "NULL");
        pin = prefs.getString("pincode", "NULL");
        landmark = prefs.getString("landmark", "NULL");
        h_no = prefs.getString("h_no", "NULL");
        st_name = prefs.getString("st_name", "NULL");


        add_to_cart = (ImageView) findViewById(R.id.add_to_cart);

        headerView = navigationView.getHeaderView(0);
        header_main = (RelativeLayout) headerView.findViewById(R.id.header_main);
        nametxt = (TextView) headerView.findViewById(R.id.initial);
        if(!name.equalsIgnoreCase("NULL")) {
            char initial = name.charAt(0);
            nametxt.setText(String.valueOf(initial).toUpperCase());
            nametxt.setTextSize(50);
        }
        header_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!name.equalsIgnoreCase("NULL")) {
                   Intent i = new Intent(NavigationDrawer.this,ActivityProfile.class);
                    startActivity(i);
                }else{
                    Intent i = new Intent(NavigationDrawer.this,WelcomeActivity.class);
                    startActivity(i);
                    finish();
                }
            }
        });

        cart_count_lay = (LinearLayout) findViewById(R.id.cart_count_lay);
        cartCount = (TextView) findViewById(R.id.cartCount);

        if(Constants.CART_COUNT == 0){
            cart_count_lay.setVisibility(View.GONE);
        }else {
            cartCount.setText(String.valueOf(Constants.CART_COUNT));
            cart_count_lay.setVisibility(View.VISIBLE);
        }
        add_to_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(NavigationDrawer.this,ActivityCart.class);
                i.putExtra("callfrom","cartbtn");
                startActivity(i);

            }
        });
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.home) {
            // Handle the camera action
            Log.e("ISVISIBLE ", ""+MainActivity.isVisible);
            if(MainActivity.isVisible == false){
                Intent i = new Intent(NavigationDrawer.this, MainActivity.class);
                startActivity(i);
                finish();
            }
        } else if (id == R.id.products) {

            if(ActivityMenu.isVisible == false){
                Intent i = new Intent(NavigationDrawer.this, ActivityMenu.class);
                startActivity(i);
                //finish();
            }
        } else if (id == R.id.services) {
            Intent i = new Intent(NavigationDrawer.this, ActivityServices.class);
            startActivity(i);
        } else if (id == R.id.delivery) {

            Intent i = new Intent(NavigationDrawer.this, ActivityDelivery.class);
            startActivity(i);

        } else if (id == R.id.offer) {

            Log.e("ISVISIBLE ", ""+ ActivityOffers.isVisible);
            if(ActivityOffers.isVisible == false){
                Intent i = new Intent(NavigationDrawer.this, ActivityOffers.class);
                startActivity(i);
                //finish();
            }

        } else if (id == R.id.about_us) {

            Intent i = new Intent(NavigationDrawer.this, ActivityAboutUs.class);
            startActivity(i);
            //finish();
        }else if (id == R.id.nav_share) {

        }else if (id == R.id.nav_rate) {

        }

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
