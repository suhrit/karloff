package com.inventrice.karloff;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.inventrice.karloff.Util.Constants;
import com.inventrice.karloff.Util.NavigationDrawer;

/**
 * Created by suhrit on 22-10-2017.
 */

public class ActivityOffers extends NavigationDrawer {

    public static boolean isVisible = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offers);
        super.init();

    }

    @Override
    public void onResume(){
        super.onResume();
        isVisible = true;

        cart_count_lay = (LinearLayout) findViewById(R.id.cart_count_lay);
        cartCount = (TextView) findViewById(R.id.cartCount);

        if(Constants.CART_COUNT == 0){
            cart_count_lay.setVisibility(View.GONE);
        }else {
            cartCount.setText(String.valueOf(Constants.CART_COUNT));
            cart_count_lay.setVisibility(View.VISIBLE);
        }

    }
    @Override
    public void onStop(){
        super.onStop();
        isVisible = false;
    }
}
