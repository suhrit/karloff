package com.inventrice.karloff;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.inventrice.karloff.Model.ModelCartItems;
import com.inventrice.karloff.Model.ModelOfflineCart;
import com.inventrice.karloff.Model.ModelProduct;
import com.inventrice.karloff.Util.Constants;
import com.inventrice.karloff.Util.NavigationDrawer;
import com.inventrice.karloff.database.DatabaseAdapter;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * Created by suhrit on 23-09-2017.
 */

public class ActivityCategoryOtherThanPizza extends NavigationDrawer implements SwipeRefreshLayout.OnRefreshListener {

    public static String catID;
    RecyclerView lv;
    static String name = null;
    static String email=null;
    static String phn=null;
    static DatabaseAdapter dbHelper;
    ArrayList<ModelProduct> productArrayList = new ArrayList<ModelProduct>();
    ProductAdapter adapter;
    ProgressDialog pd;
    CoordinatorLayout main;
    ProgressBar progressBar;
    String product_name,product_desc,rate,qty,full_amount,order_id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_other_menu);
        super.init();

        dbHelper = new DatabaseAdapter(ActivityCategoryOtherThanPizza.this);
        dbHelper.open();

        SharedPreferences prefs = getSharedPreferences("login_pref", MODE_PRIVATE);


        name = prefs.getString("name", "NULL");//"No name defined" is the default value.
        email = prefs.getString("email", "NULL"); //0 is the default value.
        phn = prefs.getString("phn", "NULL");

        main = (CoordinatorLayout) findViewById(R.id.main);
        catID = getIntent().getStringExtra("CatID");
        lv = (RecyclerView) findViewById(R.id.productlist);



        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ActivityCategoryOtherThanPizza.this);
        lv .setLayoutManager(mLayoutManager);
        lv.setNestedScrollingEnabled(false);
        lv.setVisibility(View.VISIBLE);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        FetchProduct();

    }

    public void FetchProduct(){

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.PRODUCT_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Toast.makeText(mContext,response,Toast.LENGTH_LONG).show();
                        Log.e("response ** ",response);
                        productArrayList = new ArrayList<ModelProduct>();
                        try {
                            JSONObject jsonObj = new JSONObject(response);
                            if(jsonObj.getString("status").equals("1")){
                                for(int i = 0; i < jsonObj.getJSONArray("result").length();i++){
                                    ModelProduct mp = new ModelProduct();
                                    JSONObject js = jsonObj.getJSONArray("result").getJSONObject(i);
                                    mp.setId(js.getString("id"));
                                    mp.setName(js.getString("product_name"));
                                    mp.setDesc(js.getString("product_desc"));
                                    mp.setImage(js.getString("product_img_path"));
                                    mp.setLarger_price(js.getString("product_mrp_r"));
                                    mp.setMedium_price(js.getString("product_mrp_m"));
                                    mp.setRegular_price(js.getString("product_mrp_l"));
                                    mp.setCount(1);

                                    productArrayList.add(mp);
                                }

                                adapter=new ProductAdapter(ActivityCategoryOtherThanPizza.this, productArrayList);
                                lv.setAdapter(adapter);
                                progressBar.setVisibility(View.GONE);

                            }else{
                                progressBar.setVisibility(View.GONE);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressBar.setVisibility(View.GONE);
                        }
                        //ProductAdapter adapter=new ProductAdapter(getActivity(), productArrayList);
                        //lv.setAdapter(adapter);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressBar.setVisibility(View.GONE);
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("catID",catID);
                Log.e("params==",""+params);
                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(ActivityCategoryOtherThanPizza.this);
        requestQueue.add(stringRequest);
    }

    @Override
    public void onRefresh() {
        FetchProduct();
    }

    public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ProductAdapterHolder>{
        private Context context;
        private List<ModelProduct> productlist;

        public ProductAdapter(Context context,List<ModelProduct> productlist) {
            // TODO Auto-generated constructor stub

            this.context = context;
            this.productlist = productlist;


            //Log.e("ksdf cons","ldhf");
        }
        @Override
        public ProductAdapter.ProductAdapterHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_list_row_other_menu, parent, false);
            //Log.e("ksdf","ldhf");
            return new ProductAdapterHolder(view);
        }

        @Override
        public void onBindViewHolder(final ProductAdapter.ProductAdapterHolder holder, final int position) {


            holder.productName.setText(productlist.get(position).getName());
            holder.prod_desc.setText(productlist.get(position).getDesc());
            //holder.price.setText("Rs " + productlist.get(position).getMedium_price());
            holder.med_price.setText(productlist.get(position).getMedium_price());
            holder.lar_price.setText(productlist.get(position).getLarger_price());
            holder.reg_price.setText(productlist.get(position).getRegular_price());

            holder.med_price.setTextColor(context.getResources().getColor(R.color.siteGreenColor));
            productlist.get(position).setprice(Integer.valueOf(productlist.get(position).getMedium_price()));

            Log.e("price ",productlist.get(position).getMedium_price());
            if(!productlist.get(position).getMedium_price().equals("0")) {
                productlist.get(position).setprice(Integer.valueOf(productlist.get(position).getMedium_price()));
                holder.price.setText("Rs " + Double.valueOf(productlist.get(position).getMedium_price()));
                holder.med_chk.setChecked(true);
            }else if(!productlist.get(position).getLarger_price().equals("0")) {
                productlist.get(position).setprice(Integer.valueOf(productlist.get(position).getLarger_price()));
                holder.price.setText("Rs " + Double.valueOf(productlist.get(position).getLarger_price()));
                holder.lar_chk.setChecked(true);
            }else if(!productlist.get(position).getRegular_price().equals("0")) {
                productlist.get(position).setprice(Integer.valueOf(productlist.get(position).getRegular_price()));
                holder.price.setText("Rs " + Double.valueOf(productlist.get(position).getRegular_price()));
                holder.reg_chk.setChecked(true);
            }

            Log.e("img == ",Constants.BASE_URL + "/" + productlist.get(position).getImage());
            String prod_img = Constants.BASE_URL + "/" + productlist.get(position).getImage();
            prod_img = prod_img.replaceAll(" ", "%20");
            Log.e("prod_img ",prod_img);
            Picasso.with(context).load(prod_img).into(holder.product_image);



            holder.med_chk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    productlist.get(position).setprice(Integer.valueOf(productlist.get(position).getMedium_price()));
                    holder.price.setText("Rs "+calculatePrice(productlist.get(position).getCount(),productlist.get(position).getPrice(),productlist.get(position).getCal_toppings_price()));
                    holder.lar_chk.setChecked(false);
                    holder.reg_chk.setChecked(false);

                }
            });
            holder.lar_chk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    productlist.get(position).setprice(Integer.valueOf(productlist.get(position).getLarger_price()));
                    holder.price.setText("Rs "+calculatePrice(productlist.get(position).getCount(),productlist.get(position).getPrice(),productlist.get(position).getCal_toppings_price()));
                    holder.med_chk.setChecked(false);
                    holder.reg_chk.setChecked(false);

                }
            });
            holder.reg_chk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    productlist.get(position).setprice(Integer.valueOf(productlist.get(position).getRegular_price()));
                    holder.price.setText("Rs "+calculatePrice(productlist.get(position).getCount(),productlist.get(position).getPrice(),productlist.get(position).getCal_toppings_price()));
                    holder.lar_chk.setChecked(false);
                    holder.med_chk.setChecked(false);

                }
            });


            holder.minus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(productlist.get(position).getCount()>1){
                        //int count;
                        productlist.get(position).setCount(productlist.get(position).getCount()-1);
                        holder.txt_counter.setText(String.valueOf(productlist.get(position).getCount()));
                        holder.price.setText("Rs "+calculatePrice(productlist.get(position).getCount(),productlist.get(position).getPrice(),productlist.get(position).getCal_toppings_price()));

                    }
                }
            });

            // Log.e("plus position out ",""+position+" "+productlist.get(position).getPrice());
            holder.plus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //if(productlist.get(position).getCount() != 0){
                    //int count;
                    // Log.e("plus position ",""+position+" "+productlist.get(position).getPrice());
                    productlist.get(position).setCount(productlist.get(position).getCount()+1);
                    holder.txt_counter.setText(String.valueOf(productlist.get(position).getCount()));
                    holder.price.setText("Rs "+calculatePrice(productlist.get(position).getCount(),productlist.get(position).getPrice(),productlist.get(position).getCal_toppings_price()));

                }
            });

            /*holder.add_to_cart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(!email.equalsIgnoreCase("NULL")) {
                        if (productlist.get(position).isAddToCart() == true) {
                            //Log.e("IF","TRUE");
                            holder.add_to_cart_txt.setText("Add To Cart");
                            productlist.get(position).setAddToCart(false);
                        } else {
                            // Log.e("ELSE","TRUE");
                            holder.add_to_cart_txt.setText("Item Added");
                            productlist.get(position).setAddToCart(true);
                        }
                    }else{
                        Intent i = new Intent(context, WelcomeActivity.class);
                        context.startActivity(i);
                        ((Activity)context).finish();
                    }
                }
            });*/

            holder.add_to_cart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Log.e("ELSE","TRUE "+holder.med_chk.isChecked());
                    //holder.add_to_cart_txt.setText("Item Added");
                    //productlist.get(position).setAddToCart(true);
                    product_name = productlist.get(position).getName();
                    product_desc = productlist.get(position).getDesc();
                    String prod_id = productlist.get(position).getId();
                    //order_id = productlist.get(position).getOrder_id();
                    if(holder.med_chk.isChecked()) {
                        rate = productlist.get(position).getMedium_price();
                        qty = holder.txt_counter.getText().toString();
                        full_amount = String.valueOf(Double.valueOf(qty)*Double.valueOf(rate));
                        Log.e("IF ","TRUE");
                    }else if(holder.lar_chk.isChecked()) {
                        rate = productlist.get(position).getLarger_price();
                        qty = holder.txt_counter.getText().toString();
                        full_amount = String.valueOf(Double.valueOf(qty)*Double.valueOf(rate));
                        Log.e("ELSE IF ","TRUE");
                    } else if(holder.reg_chk.isChecked()) {
                        rate = productlist.get(position).getRegular_price();
                        qty = holder.txt_counter.getText().toString();
                        full_amount = String.valueOf(Double.valueOf(qty)*Double.valueOf(rate));
                        Log.e("ELSE ","TRUE");
                    }
                    Log.e("RATE ",rate);
                    Log.e("qty ",qty);
                    Log.e("full_amount ",full_amount);
                    Log.e("product_name ",product_name);
                    Log.e("product_desc ",product_desc);
                    //Log.e("order_id ",order_id);

                    String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
                    Long tsLong = System.currentTimeMillis()/1000;
                    String ts = tsLong.toString();
                    Log.e("date ",date+" "+ts);

                    if(!email.equalsIgnoreCase("NULL")) {

                        Constants.CART_COUNT = Constants.CART_COUNT+1;
                        AddToCart(prod_id,product_name,product_desc,rate,qty,full_amount,date,ts,"cart");


                        //}
                    }else{
                        /*Intent i = new Intent(context, WelcomeActivity.class);
                        context.startActivity(i);
                        ((Activity)context).finish();*/


                        ModelOfflineCart moc = new ModelOfflineCart();
                        moc.setId(prod_id);
                        moc.setName(product_name);
                        moc.setDesc(product_desc);
                        moc.setDate(date);
                        moc.setQty(qty);
                        moc.setPrice(full_amount);
                        moc.setRate(rate);
                        moc.setTime(ts);
                        moc.setExtrChzPrice("0");
                        moc.setChzStat("false");

                        boolean insertcart = dbHelper.InsertItemToCart(moc);

                        Constants.CART_COUNT = Constants.CART_COUNT + 1;

                        Log.e("insertcart ",""+insertcart);

                        cart_count_lay = (LinearLayout) findViewById(R.id.cart_count_lay);
                        cartCount = (TextView) findViewById(R.id.cartCount);

                        if(Constants.CART_COUNT == 0){
                            cart_count_lay.setVisibility(View.GONE);
                        }else {
                            cartCount.setText(String.valueOf(Constants.CART_COUNT));
                            cart_count_lay.setVisibility(View.VISIBLE);
                        }

                    }
                }
            });

            holder.buyNow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    /*Log.e("ELSE","TRUE "+holder.med_chk.isChecked());

                    product_name = productlist.get(position).getName();
                    product_desc = productlist.get(position).getDesc();
                    String prod_id = productlist.get(position).getId();
                    Random r =new Random();

                    order_id = String.valueOf((long)(r.nextDouble()*10000000000L));
                    Log.e("order_id ",order_id);
                    if(holder.med_chk.isChecked()) {
                        rate = productlist.get(position).getMedium_price();
                        qty = holder.txt_counter.getText().toString();
                        full_amount = String.valueOf(Double.valueOf(qty)*Double.valueOf(rate));
                        Log.e("IF ","TRUE");
                    }else if(holder.lar_chk.isChecked()) {
                        rate = productlist.get(position).getLarger_price();
                        qty = holder.txt_counter.getText().toString();
                        full_amount = String.valueOf(Double.valueOf(qty)*Double.valueOf(rate));
                        Log.e("ELSE IF ","TRUE");
                    } else if(holder.reg_chk.isChecked()) {
                        rate = productlist.get(position).getRegular_price();
                        qty = holder.txt_counter.getText().toString();
                        full_amount = String.valueOf(Double.valueOf(qty)*Double.valueOf(rate));
                        Log.e("ELSE ","TRUE");
                    }
                    Log.e("RATE ",rate);
                    Log.e("qty ",qty);
                    Log.e("full_amount ",full_amount);
                    Log.e("product_name ",product_name);
                    Log.e("product_desc ",product_desc);
                    //Log.e("order_id ",order_id);

                    String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
                    Long tsLong = System.currentTimeMillis()/1000;
                    String ts = tsLong.toString();
                    Log.e("date ",date+" "+ts);

                    if(!email.equalsIgnoreCase("NULL")) {
                        //BuyNow(prod_id,product_name,product_desc,rate,qty,full_amount,date,ts);
                        ModelCartItems mci = new ModelCartItems();
                        mci.setName(product_name);
                        mci.setDesc(product_desc);
                        mci.setPrice(full_amount);
                        mci.setQty(qty);
                        mci.setRate(rate);
                        mci.setId(prod_id);
                        mci.setOrder_id(order_id);

                        Constants.offlineCartItems.add(mci);

                        Intent i =  new Intent(ActivityCategoryOtherThanPizza.this,ActivityCart.class);
                        i.putExtra("callfrom","buynow");
                        startActivity(i);
                    }else {
                        Intent i = new Intent(context, WelcomeActivity.class);
                        context.startActivity(i);
                        ((Activity) context).finish();
                    }*/


                    Log.e("ELSE","TRUE "+holder.med_chk.isChecked());
                    //holder.add_to_cart_txt.setText("Item Added");
                    //productlist.get(position).setAddToCart(true);
                    product_name = productlist.get(position).getName();
                    product_desc = productlist.get(position).getDesc();
                    String prod_id = productlist.get(position).getId();
                    //order_id = productlist.get(position).getOrder_id();
                    if(holder.med_chk.isChecked()) {
                        rate = productlist.get(position).getMedium_price();
                        qty = holder.txt_counter.getText().toString();
                        full_amount = String.valueOf(Double.valueOf(qty)*Double.valueOf(rate));
                        Log.e("IF ","TRUE");
                    }else if(holder.lar_chk.isChecked()) {
                        rate = productlist.get(position).getLarger_price();
                        qty = holder.txt_counter.getText().toString();
                        full_amount = String.valueOf(Double.valueOf(qty)*Double.valueOf(rate));
                        Log.e("ELSE IF ","TRUE");
                    } else if(holder.reg_chk.isChecked()) {
                        rate = productlist.get(position).getRegular_price();
                        qty = holder.txt_counter.getText().toString();
                        full_amount = String.valueOf(Double.valueOf(qty)*Double.valueOf(rate));
                        Log.e("ELSE ","TRUE");
                    }
                    Log.e("RATE ",rate);
                    Log.e("qty ",qty);
                    Log.e("full_amount ",full_amount);
                    Log.e("product_name ",product_name);
                    Log.e("product_desc ",product_desc);
                    //Log.e("order_id ",order_id);

                    String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
                    Long tsLong = System.currentTimeMillis()/1000;
                    String ts = tsLong.toString();
                    Log.e("date ",date+" "+ts);

                    if(!email.equalsIgnoreCase("NULL")) {

                        Constants.CART_COUNT = Constants.CART_COUNT+1;
                        AddToCart(prod_id,product_name,product_desc,rate,qty,full_amount,date,ts, "buynow");


                        //}
                    }else{
                        /*Intent i = new Intent(context, WelcomeActivity.class);
                        context.startActivity(i);
                        ((Activity)context).finish();*/


                        ModelOfflineCart moc = new ModelOfflineCart();
                        moc.setId(prod_id);
                        moc.setName(product_name);
                        moc.setDesc(product_desc);
                        moc.setDate(date);
                        moc.setQty(qty);
                        moc.setPrice(full_amount);
                        moc.setRate(rate);
                        moc.setTime(ts);
                        moc.setExtrChzPrice("0");
                        moc.setChzStat("false");

                        boolean insertcart = dbHelper.InsertItemToCart(moc);

                        Constants.CART_COUNT = Constants.CART_COUNT + 1;

                        Log.e("insertcart ",""+insertcart);

                        cart_count_lay = (LinearLayout) findViewById(R.id.cart_count_lay);
                        cartCount = (TextView) findViewById(R.id.cartCount);

                        if(Constants.CART_COUNT == 0){
                            cart_count_lay.setVisibility(View.GONE);
                        }else {
                            cartCount.setText(String.valueOf(Constants.CART_COUNT));
                            cart_count_lay.setVisibility(View.VISIBLE);
                        }

                    }
                }
            });

        }

        @Override
        public int getItemCount() {
            return productlist.size();
        }

        class ProductAdapterHolder extends RecyclerView.ViewHolder {
            TextView productName,prod_desc,price,med_price,lar_price,reg_price,add_to_cart_txt,toppings_txt,txt_counter;
            ImageView product_image,minus,plus;
            CheckBox med_chk,lar_chk,reg_chk,extraChz_chk;
            LinearLayout add_to_cart,buyNow;
            public ProductAdapterHolder(View v) {
                super(v);

                productName = (TextView) v.findViewById(R.id.productName);
                prod_desc = (TextView) v.findViewById(R.id.prod_desc);
                price = (TextView) v.findViewById(R.id.price);
                med_price = (TextView) v.findViewById(R.id.med_price);
                lar_price = (TextView) v.findViewById(R.id.lar_price);
                reg_price = (TextView) v.findViewById(R.id.reg_price);
                txt_counter = (TextView) v.findViewById(R.id.txt_counter);
                add_to_cart_txt = (TextView) v.findViewById(R.id.add_to_cart_txt);


                product_image = (ImageView) v.findViewById(R.id.product_image);
                minus = (ImageView) v.findViewById(R.id.minus);
                plus = (ImageView) v.findViewById(R.id.plus);

                med_chk = (CheckBox) v.findViewById(R.id.med_chk);
                lar_chk = (CheckBox) v.findViewById(R.id.lar_chk);
                reg_chk = (CheckBox) v.findViewById(R.id.reg_chk);
                extraChz_chk = (CheckBox) v.findViewById(R.id.extraChz_chk);


                add_to_cart = (LinearLayout) v.findViewById(R.id.add_to_cart);
                buyNow = (LinearLayout) v.findViewById(R.id.buyNow);
            }
        }

        /*public interface OnItemClickListener {
            public void onItemClick(View view, int position);
        }*/
        public double calculatePrice(int count,int price, double toppings_price){
            double total;
            Log.e("cal == ",""+count+" * "+price+" + "+toppings_price);
            total = (count*price)+toppings_price;
            Log.e("TOTAL = ",""+total);
            //holder.price.setText("Rs "+productlist.get(position).getPrice());
            return total;
        }
    }

    public void AddToCart(final String product_id, final String product_name, final String product_desc, final String rate, final String qty, final String full_amount, final String date, final String timestamp, final String tag){
        pd = new ProgressDialog(ActivityCategoryOtherThanPizza.this,ProgressDialog.THEME_HOLO_DARK);
        pd.setMessage("Please Wait...");
        pd.setCancelable(false);
        pd.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.ADDTOCART,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Toast.makeText(mContext,response,Toast.LENGTH_LONG).show();
                        Log.e("response ** ",response);
                        try {
                            JSONObject jsonObj = new JSONObject(response);
                            if(jsonObj.getString("status").equals("1")){

                                Snackbar snackbar = Snackbar
                                        .make(main, "Item Added To Cart", Snackbar.LENGTH_LONG);

                                snackbar.show();


                                pd.hide();
                                if(tag.equalsIgnoreCase("buynow")){
                                    Intent i = new Intent(ActivityCategoryOtherThanPizza.this,ActivityCart.class);
                                    startActivity(i);
                                }else {
                                    //finish();
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            cart_count_lay = (LinearLayout) findViewById(R.id.cart_count_lay);
                                            cartCount = (TextView) findViewById(R.id.cartCount);

                                            if (Constants.CART_COUNT == 0) {
                                                cart_count_lay.setVisibility(View.GONE);
                                            } else {
                                                cartCount.setText(String.valueOf(Constants.CART_COUNT));
                                                cart_count_lay.setVisibility(View.VISIBLE);
                                            }
                                        }
                                    });
                                }

                            }else{

                                pd.hide();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            pd.hide();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Toast.makeText(mContext,error.toString(),Toast.LENGTH_LONG).show();
                        pd.hide();
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("cust_id", phn);
                params.put("prod_type", catID);
                params.put("product_name", product_name);
                params.put("product_desc", product_desc);
                params.put("rate", rate);
                params.put("qty", qty);
                params.put("full_amount", full_amount);
                params.put("date", date);
                params.put("timestamp", timestamp);
                Log.e("params==",""+params);
                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(ActivityCategoryOtherThanPizza.this);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,-1,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    public void BuyNow(final String product_id, final String product_name, final String product_desc, final String rate, final String qty, final String full_amount, final String date, final String timestamp){

    }
}
