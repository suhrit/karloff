package com.inventrice.karloff;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.inventrice.karloff.Util.Constants;
import com.inventrice.karloff.Util.NavigationDrawer;

public class MainActivity extends NavigationDrawer
        implements View.OnClickListener {

    LinearLayout menu,offer;
    public static boolean isVisible = true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        super.init();



        offer = (LinearLayout) findViewById(R.id.offer);
        menu = (LinearLayout) findViewById(R.id.menu);

        menu.setOnClickListener(this);
        offer.setOnClickListener(this);





        //Toast.makeText(MainActivity.this,name+" "+email,Toast.LENGTH_LONG).show();
    }

    @Override
    public void onBackPressed() {
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            //super.onBackPressed();
            finish();
            /*Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();*/
        }
    }


    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.menu:
                Intent i = new Intent(MainActivity.this,ActivityMenu.class);
                startActivity(i);
                break;

            case R.id.offer:
                Intent i1 = new Intent(MainActivity.this, ActivityOffers.class);
                startActivity(i1);
                break;
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        isVisible = true;

        cart_count_lay = (LinearLayout) findViewById(R.id.cart_count_lay);
        cartCount = (TextView) findViewById(R.id.cartCount);

        if(Constants.CART_COUNT == 0){
            cart_count_lay.setVisibility(View.GONE);
        }else {
            cartCount.setText(String.valueOf(Constants.CART_COUNT));
            cart_count_lay.setVisibility(View.VISIBLE);
        }

    }
    @Override
    public void onStop(){
        super.onStop();
        isVisible = false;
    }

}
