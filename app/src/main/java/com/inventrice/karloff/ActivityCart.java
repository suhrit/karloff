package com.inventrice.karloff;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.inventrice.karloff.Model.ModelCartItems;
import com.inventrice.karloff.Util.Constants;
import com.inventrice.karloff.Util.NavigationDrawer;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by suhrit on 24-09-2017.
 */

public class ActivityCart extends AppCompatActivity{
    ProgressBar progressBar;
    CoordinatorLayout main;
    RecyclerView  lv;
    ArrayList<ModelCartItems> productArrayList;
    CartAdapter adapter;
    public String name = null,email=null,phn=null, pin = null, landmark = null, h_no = null,st_name = null;
    SharedPreferences prefs;
    LinearLayout checkout;
    TextView totalPrice,totalItems,nametxt,phntxt,emailtxt,pintxt,landmarktxt,stNametxt,housenotxt,update,emptyMsg;
    double total;
    CardView card_view3,card_infoDetails,card_addDetails;
    ProgressDialog pd;
    String orderID,pinStat,callfrom;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.app_bar_cart);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //What to do on back clicked
                finish();
            }
        });


        prefs = getSharedPreferences("login_pref", MODE_PRIVATE);

        callfrom = getIntent().getStringExtra("callfrom");
        name = prefs.getString("name", "NULL");//"No name defined" is the default value.
        email = prefs.getString("email", "NULL"); //0 is the default value.
        phn = prefs.getString("phn", "NULL");
        pin = prefs.getString("pincode", "NULL");
        landmark = prefs.getString("landmark", "NULL");
        h_no = prefs.getString("h_no", "NULL");
        st_name = prefs.getString("st_name", "NULL");

        main = (CoordinatorLayout) findViewById(R.id.main);
        lv = (RecyclerView) findViewById(R.id.productlist);



        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        lv .setLayoutManager(mLayoutManager);
        lv.setNestedScrollingEnabled(false);
        lv.setVisibility(View.VISIBLE);

        /*adapter=new ProductAdapter(getActivity(), productArrayList);
        lv.setAdapter(adapter);*/
        //ProductAdapter adapter=new ProductAdapter(getActivity(), productArrayList);
        //lv.setAdapter(adapter);


        checkout = (LinearLayout) findViewById(R.id.checkout);
        totalItems = (TextView) findViewById(R.id.totalItems);
        totalPrice = (TextView) findViewById(R.id.totalPrice);
        card_view3 = (CardView) findViewById(R.id.card_view3);
        card_infoDetails = (CardView) findViewById(R.id.card_infoDetails);
        card_addDetails = (CardView) findViewById(R.id.card_addDetails);

        nametxt = (TextView) findViewById(R.id.nametxt);
        phntxt = (TextView) findViewById(R.id.phntxt);
        emailtxt = (TextView) findViewById(R.id.emailtxt);
        pintxt = (TextView) findViewById(R.id.pintxt);
        emptyMsg = (TextView) findViewById(R.id.emptyMsg);

        housenotxt = (TextView) findViewById(R.id.housenotxt);
        stNametxt = (TextView) findViewById(R.id.stNametxt);
        landmarktxt = (TextView) findViewById(R.id.landmarktxt);

        nametxt.setText(name);
        phntxt.setText(phn);
        emailtxt.setText(email);
        pintxt.setText(pin);

        housenotxt.setText(h_no);
        stNametxt.setText(st_name);
        landmarktxt.setText(landmark);

        update = (TextView) findViewById(R.id.update);
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog = new Dialog(ActivityCart.this);
                // Include dialog.xml file
                dialog.setContentView(R.layout.update_add);
                // Set dialog title
                dialog.setTitle("Update Address");

                // set values for custom dialog components - text, image and button
                final EditText hsno = (EditText) dialog.findViewById(R.id.hsno);
                final EditText strtNo = (EditText) dialog.findViewById(R.id.strtNo);
                final EditText lndmrk = (EditText) dialog.findViewById(R.id.lndmrk);

                hsno.setText(h_no);
                strtNo.setText(st_name);
                lndmrk.setText(landmark);

                dialog.show();

                TextView cancel = (TextView) dialog.findViewById(R.id.cancel);
                // if decline button is clicked, close the custom dialog
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // Close dialog
                        dialog.dismiss();
                    }
                });

                TextView save = (TextView) dialog.findViewById(R.id.save);
                // if decline button is clicked, close the custom dialog
                save.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // Close dialog
                        UpdateAddress(hsno.getText().toString(), strtNo.getText().toString(),lndmrk.getText().toString());
                        dialog.dismiss();
                    }
                });
            }
        });

        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        if(callfrom.equalsIgnoreCase("cartbtn")) {
            GetCart();
        }else{
            progressBar.setVisibility(View.GONE);
            Log.e("buynow size ",""+Constants.offlineCartItems.size());
            if(Constants.offlineCartItems.size() > 0) {
                productArrayList = Constants.offlineCartItems;
                adapter = new CartAdapter(ActivityCart.this, productArrayList);
                lv.setAdapter(adapter);
            }
        }
        checkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ActivityCart.this,ActivityCheckout.class);
                i.putExtra("orderID", orderID);
                i.putExtra("total",String.valueOf(total));
                i.putExtra("pinStat",pinStat);
                startActivity(i);
            }
        });
    }

    private void GetCart(){

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.GET_CART_ITEMS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Toast.makeText(mContext,response,Toast.LENGTH_LONG).show();
                        Log.e("response ** ",response);

                        int sum = 0;
                        total = 0.00;
                        progressBar.setVisibility(View.GONE);
                        try {

                            JSONObject jsonObj = new JSONObject(response);
                            if(jsonObj.getString("status").equals("1")){

                                productArrayList = new ArrayList<ModelCartItems>();
                                pinStat = jsonObj.getString("pinStat");

                                Log.e("item ",""+jsonObj.getJSONArray("items").length());
                                Log.e("toppings ",""+jsonObj.getJSONArray("toppings").length());

                                sum = sum+jsonObj.getJSONArray("items").length()+jsonObj.getJSONArray("toppings").length();
                                Log.e("sum ",""+sum);
                                totalItems.setText("Total Items: "+sum);

                                Constants.CART_COUNT = sum;

                                if(sum == 0){
                                    Log.e("IF","TRUE");
                                    card_view3.setVisibility(View.GONE);
                                    card_infoDetails.setVisibility(View.GONE);
                                    card_addDetails.setVisibility(View.GONE);
                                    emptyMsg.setVisibility(View.VISIBLE);
                                }else {
                                    Log.e("ELSE","TRUE");

                                    emptyMsg.setVisibility(View.GONE);
                                    for (int i = 0; i < jsonObj.getJSONArray("items").length(); i++) {
                                        ModelCartItems mp = new ModelCartItems();
                                        JSONObject js = jsonObj.getJSONArray("items").getJSONObject(i);

                                        orderID = js.getString("orderID");
                                        mp.setId(js.getString("id"));
                                        mp.setName(js.getString("name"));
                                        mp.setDesc(js.getString("desc"));
                                        mp.setOrder_id(js.getString("orderID"));
                                        mp.setRate(js.getString("rate"));
                                        mp.setQty(js.getString("qty"));
                                        mp.setPrice(js.getString("price"));
                                        //mp.setOrder_id(js.getString("order_id"));
                                        double price = Double.valueOf(js.getString("price"));
                                        total = total + price;

                                        productArrayList.add(mp);
                                    }

                                    for (int i = 0; i < jsonObj.getJSONArray("toppings").length(); i++) {
                                        ModelCartItems mp = new ModelCartItems();
                                        JSONObject js = jsonObj.getJSONArray("toppings").getJSONObject(i);
                                        mp.setId(js.getString("id"));
                                        mp.setName(js.getString("name"));
                                        mp.setDesc(js.getString("desc"));
                                        mp.setOrder_id(js.getString("orderID"));
                                        mp.setRate(js.getString("rate"));
                                        mp.setQty(js.getString("qty"));
                                        mp.setPrice(js.getString("price"));
                                        //mp.setOrder_id(js.getString("order_id"));
                                        double price = Double.valueOf(js.getString("price"));
                                        total = total + price;
                                        productArrayList.add(mp);
                                    }

                                    totalPrice.setText(ActivityCart.this.getString(R.string.Rs) + total);


                                    //CLA.notifyDataSetChanged();
                                    card_view3.setVisibility(View.VISIBLE);
                                    card_infoDetails.setVisibility(View.VISIBLE);
                                    card_addDetails.setVisibility(View.VISIBLE);


                                }
                                Log.e("productArrayList size",""+productArrayList.size());
                                adapter = new CartAdapter(ActivityCart.this, productArrayList);
                                lv.setAdapter(adapter);
                            }else{
                                emptyMsg.setVisibility(View.VISIBLE);
                                progressBar.setVisibility(View.GONE);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            emptyMsg.setVisibility(View.VISIBLE);
                            progressBar.setVisibility(View.GONE);
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        emptyMsg.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.GONE);
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("pin",pin);
                params.put("cust_id",phn);
                Log.e("params==",""+params);
                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(ActivityCart.this);
        requestQueue.add(stringRequest);
    }

    public class CartAdapter extends RecyclerView.Adapter<CartAdapter.ProductAdapterHolder>{
        private Context context;
        private List<ModelCartItems> productlist;

        public CartAdapter(Context context,List<ModelCartItems> productlist) {
            // TODO Auto-generated constructor stub

            this.context = context;
            this.productlist = productlist;


            Log.e("ksdf cons","ldhf");
        }
        @Override
        public CartAdapter.ProductAdapterHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cart_list_row, parent, false);
            //Log.e("ksdf","ldhf");

            return new ProductAdapterHolder(view);
        }

        @Override
        public void onBindViewHolder(final CartAdapter.ProductAdapterHolder holder, final int position) {

            holder.productName.setText(productlist.get(position).getName());
            holder.prod_desc.setText(productlist.get(position).getDesc());
            holder.price.setText(context.getString(R.string.Rs)+productlist.get(position).getPrice());
            holder.rate.setText(context.getString(R.string.Rs)+productlist.get(position).getRate());
            holder.qty.setText(productlist.get(position).getQty());

            holder.del.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.e("id ",productlist.get(position).getId());
                    DeleteItem(productlist.get(position).getId());
                }
            });
        }

        @Override
        public int getItemCount() {
            return productlist.size();
        }

        class ProductAdapterHolder extends RecyclerView.ViewHolder {
            TextView productName,prod_desc,price,rate,qty;
            LinearLayout del;

            public ProductAdapterHolder(View v) {
                super(v);

                productName = (TextView) v.findViewById(R.id.name);
                prod_desc = (TextView) v.findViewById(R.id.desc);
                price = (TextView) v.findViewById(R.id.price);
                rate = (TextView) v.findViewById(R.id.rate);
                qty = (TextView) v.findViewById(R.id.qty);

                del = (LinearLayout)v.findViewById(R.id.del);

            }
        }

    }

    public void UpdateAddress(final String hsno, final String strtNo, final String lndmrk){

        pd = new ProgressDialog(ActivityCart.this,ProgressDialog.THEME_HOLO_DARK);
        pd.setMessage("Please Wait...");
        pd.setCancelable(false);
        pd.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.UPDATE_ADDRESS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Toast.makeText(mContext,response,Toast.LENGTH_LONG).show();
                        Log.e("response ** ",response);

                        try {

                            JSONObject jsonObj = new JSONObject(response);
                            if(jsonObj.getString("status").equals("1")) {
                                SharedPreferences mySPrefs = getSharedPreferences("login_pref", MODE_PRIVATE);
                                SharedPreferences.Editor editor = mySPrefs.edit();
                                editor.remove(h_no);
                                editor.remove(landmark);
                                editor.remove(st_name);
                                editor.apply();

                                editor.putString("h_no", jsonObj.getString("h_no"));
                                editor.putString("landmark", jsonObj.getString("landmark"));
                                editor.putString("st_name", jsonObj.getString("st_no"));
                                editor.apply();

                                housenotxt.setText(h_no);
                                stNametxt.setText(st_name);
                                landmarktxt.setText(landmark);
                                pd.hide();
                                finish();
                                startActivity(getIntent());
                            }else{
                                Snackbar snackbar = Snackbar
                                        .make(main, jsonObj.getString("message"), Snackbar.LENGTH_LONG);

                                snackbar.show();
                                pd.hide();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            pd.hide();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Toast.makeText(mContext,error.toString(),Toast.LENGTH_LONG).show();

                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("h_no",hsno);
                params.put("mobile",phn);
                params.put("st_no",strtNo);
                params.put("landmark",lndmrk);
                Log.e("params==",""+params);
                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(ActivityCart.this);
        requestQueue.add(stringRequest);
    }

    public void DeleteItem(final String id){

        pd = new ProgressDialog(ActivityCart.this,ProgressDialog.THEME_HOLO_DARK);
        pd.setMessage("Please Wait...");
        pd.setCancelable(false);
        pd.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.DELETE_CART_ITEMS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Toast.makeText(mContext,response,Toast.LENGTH_LONG).show();
                        Log.e("response ** ",response);

                        try {

                            JSONObject jsonObj = new JSONObject(response);
                            if(jsonObj.getString("status").equals("1")) {
                                Snackbar snackbar = Snackbar
                                        .make(main, jsonObj.getString("message"), Snackbar.LENGTH_LONG);

                                snackbar.show();
                                pd.hide();
                                GetCart();
                            }else{
                                Snackbar snackbar = Snackbar
                                        .make(main, jsonObj.getString("message"), Snackbar.LENGTH_LONG);

                                snackbar.show();
                                pd.hide();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            pd.hide();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Toast.makeText(mContext,error.toString(),Toast.LENGTH_LONG).show();

                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("id",id);

                Log.e("params==",""+params);
                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(ActivityCart.this);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }
}
