package com.inventrice.karloff;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.inventrice.karloff.Model.ModelCartItems;
import com.inventrice.karloff.Util.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by suhrit on 22-10-2017.
 */

public class AccountOrders extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener{

    ListView lv;
    public SwipeRefreshLayout swipeRefreshLayout;
    ArrayList<ModelCartItems> productArrayList;
    public String name = null,email=null,phn=null, pin = null, landmark = null, h_no = null,st_name = null;
    SharedPreferences prefs;
    OrderListAdapter OLA;
    TextView empty;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_orders);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //What to do on back clicked
                finish();
            }
        });

        prefs = getSharedPreferences("login_pref", MODE_PRIVATE);


        name = prefs.getString("name", "NULL");//"No name defined" is the default value.
        email = prefs.getString("email", "NULL"); //0 is the default value.
        phn = prefs.getString("phn", "NULL");
        pin = prefs.getString("pincode", "NULL");
        landmark = prefs.getString("landmark", "NULL");
        h_no = prefs.getString("h_no", "NULL");
        st_name = prefs.getString("st_name", "NULL");

        empty = (TextView) findViewById(R.id.empty);
        lv = (ListView) findViewById(R.id.lv);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.content_menu);


        swipeRefreshLayout.setOnRefreshListener(this);

        swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        swipeRefreshLayout.setRefreshing(true);

                                        FetchOrders();
                                    }
                                }
        );

    }

    private void FetchOrders(){

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.MY_ORDERS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Toast.makeText(mContext,response,Toast.LENGTH_LONG).show();
                        Log.e("response ** ",response);
                        productArrayList = new ArrayList<ModelCartItems>();
                        try {
                            JSONObject jsonObj = new JSONObject(response);
                            if(jsonObj.getString("status").equals("1")){
                                empty.setVisibility(View.GONE);
                                lv.setVisibility(View.VISIBLE);

                                for(int i = 0; i < jsonObj.getJSONArray("items").length();i++){
                                    ModelCartItems mc = new ModelCartItems();
                                    JSONObject js = jsonObj.getJSONArray("items").getJSONObject(i);
                                    mc.setId(js.getString("id"));
                                    mc.setName(js.getString("name"));
                                    mc.setDesc(js.getString("desc"));
                                    mc.setOrder_id(js.getString("order_id"));
                                    mc.setRate(js.getString("rate"));
                                    mc.setQty(js.getString("qty"));
                                    mc.setPrice(js.getString("price"));


                                    productArrayList.add(mc);
                                }
                                OLA = new OrderListAdapter(AccountOrders.this, productArrayList);
                                lv.setAdapter(OLA);
                                //CLA.notifyDataSetChanged();
                                swipeRefreshLayout.setRefreshing(false);
                            }else{
                                empty.setVisibility(View.VISIBLE);
                                lv.setVisibility(View.GONE);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            empty.setVisibility(View.VISIBLE);
                            lv.setVisibility(View.GONE);
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Toast.makeText(mContext,error.toString(),Toast.LENGTH_LONG).show();
                        swipeRefreshLayout.setRefreshing(false);
                        empty.setVisibility(View.VISIBLE);
                        lv.setVisibility(View.GONE);
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("cust_id", phn);



                Log.e("params==",""+params);
                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(AccountOrders.this);
        requestQueue.add(stringRequest);
    }
    @Override
    public void onRefresh() {
        FetchOrders();
    }

    public class OrderListAdapter extends BaseAdapter{

        Context context;
        ArrayList<ModelCartItems> list;
        public OrderListAdapter(Context context, ArrayList<ModelCartItems> list){
            this.context = context;
            this.list = list;
        }
        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int i) {
            return list.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            if (view == null) {
                LayoutInflater inflater = (LayoutInflater) context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.order_list_row, viewGroup, false);

                TextView productName,prod_desc,price,rate,qty;
                productName = (TextView) view.findViewById(R.id.name);
                prod_desc = (TextView) view.findViewById(R.id.desc);
                price = (TextView) view.findViewById(R.id.price);
                rate = (TextView) view.findViewById(R.id.rate);
                qty = (TextView) view.findViewById(R.id.qty);

                productName.setText(list.get(i).getName());
                prod_desc.setText(list.get(i).getDesc());
                price.setText(context.getString(R.string.Rs)+list.get(i).getPrice());
                rate.setText(context.getString(R.string.Rs)+list.get(i).getRate());
                qty.setText(list.get(i).getQty());
            }
            return view;
        }
    }
}
