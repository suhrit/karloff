package com.inventrice.karloff;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.inventrice.karloff.Util.PicassoLoader;

import java.util.ArrayList;

import cn.lightsky.infiniteindicator.IndicatorConfiguration;
import cn.lightsky.infiniteindicator.InfiniteIndicator;
import cn.lightsky.infiniteindicator.OnPageClickListener;
import cn.lightsky.infiniteindicator.Page;

/**
 * Created by suhrit on 23-09-2017.
 */

public class ActivityDelivery extends AppCompatActivity implements ViewPager.OnPageChangeListener,OnPageClickListener {

    private InfiniteIndicator mAnimLineIndicator;
    private ArrayList<Page> pageViews;
    TextView text;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_aboutus);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //What to do on back clicked
                finish();
            }
        });

        text = (TextView) findViewById(R.id.text);
        text.setText(getResources().getString(R.string.delivery));
        initData();
        testAnimLineIndicator();
    }
    private void initData() {
        pageViews = new ArrayList<>();

        pageViews.add(new Page("A", R.drawable.cat_pizza, this));
        pageViews.add(new Page("B", R.drawable.cat_combo_meal, this));
        pageViews.add(new Page("C", R.drawable.cat_fried_chicken, this));


    }

    private void testAnimLineIndicator() {
        mAnimLineIndicator = (InfiniteIndicator) findViewById(R.id.infinite_anim_line);
        IndicatorConfiguration configuration = new IndicatorConfiguration.Builder()
                .imageLoader(new PicassoLoader())
                .isAutoScroll(true)
                .isStopWhileTouch(true)
                .onPageChangeListener(this)
                .position(IndicatorConfiguration.IndicatorPosition.Center_Bottom)
                .build();
        mAnimLineIndicator.init(configuration);
        mAnimLineIndicator.notifyDataChange(pageViews);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onPageClick(int position, Page page) {

    }
}



